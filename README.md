# DProbustness tool & library: _compute exact __PSTN__ robustness_

This is the library used for the experiments in the paper 

>_**Robustness Computation of Dynamic Controllability  
in Probabilistic Temporal Networks with Ordinary Distributions**_  
>_Michael Saint-Guillain\*, Tiago Vaquero\*\*, Jagriti Agrawal\*\*, Steve Chien\*\*, 2020_  
>_29th International Joint Conference on Artificial Intelligence (IJCAI'20)_
>
>_\*  Universit� catholique de Louvain, Belgium_  
>_\*\* Jet Propulsion Laboratory, California Institute Technology, CA, USA_



---

## What is a PSTN ? 

PSTN stands for _Probabilistic Simple Temporal Networks_.

__Temporal networks__ formalize the arrangement and inter-dependencies of tasks, or activities, that compose an operational project. 
In a simple temporal network (STN), activities are modeled as a finite set of time events, such as start and end times. 

In practice, some activity durations, considered as _contingent_, remains unknown beforehand.
In the case some stochastic knowledge on the uncertain durations exists, then one can model it as (estimated) probability distributions, leading to the extending concept of _probabilistic STN_, or __PSTN__.

Solving a (P)STN then amounts at finding an assignment of time values to events that fulfils all the constraints between events (_e.g._, end of task _A_ must happen between 10 and 20 minutes before the beginning of _B_).
Whenever such schedule exists, a network is said to be _controllable_.
When the operational assumptions enable it, the schedule may be _dynamically constructed_, the time values being assigned as durations are observed.
Yet, even under dynamic decision, due to unfortunate durations a network may reveal uncontrollable.
How likely is a PSTN to lead to a successful execution? 

### Illustrative example: planetery rover operations
Consider the following hypothetical example of a Mars rovers PSTN:  

![Rovers example](/img/Rovers_example.png)

Each rover has three activities in sequence: drive towards a science site, experiment, and relay results to an orbiter. 
A special time point _t0 = 0_ represents the beginning of the operations, at _time unit (*tu*) 0_. 
Time events are linked by temporal constraints (arrow), either controllable (plain) or contingent (dash). 
In this example, the rovers work independently during their driving and science activities.
They do not coordinate until the communication time window, which strictly happens between time 600 to 700.
Communication tasks cannot overlap, and _**Rover1**_ is chosen to relay first, _**Rover2**_ second. 
However, duration of driving and experimental activities are highly uncertain.
In practice, distributions can be estimated from historical observations. 
The probability distributions _X_ describe the stochastic duration of driving and experimental activities.

## Dynamic controllability, robustness and dispatching protocols 

Provided some stochastic knowledge on contingent activity durations, the _degree of dynamic controllability_ (__DDC__) of a PSTN can be quantified, as the _success probability_, or __robustness__, of a given task network. 

This tool computes the probability that, executing _NextFirst_ dispatching protocol in order to dynamically decide when to start each activity, leads to a successful execution (_i.e._ in our example, the two rovers find enough time �20*tu*'s for _**Rover1**_, 30*tu*'s for _**Rover2**_� to relay within the communication interval 600-700).

## _NextFirst_ dispatching protocol, or _what does this DProbustness tool compute_?

The more clever one can do would be to rely on dynamic re-optimization in order to take the best decisions (when to start each activity � and end it in case of a controllable duration such as _Rover1::relay_), as the contingent durations are observed. 
However, this is not always (actually _rarely_) possible, usually because of computational limitations. This is particularly the case for planetary rovers. 

The _NextFirst_ dispatching protocol (DP) simply consists in starting each activity as soon as possible. For instance, _t11_ would be started as soon as both _Rover2:expe_ and _Rover1:relay_ + 5 _tu_'s ended. Also, _Rover1:relay_ will last for 20 _tu_'s, although it could be up to 30, _NextFirst_ always chooses the minimum. 
Such a simple, yet quite natural, assumption as "start each activity as soon as possible" enable an efficient computation of the success probability of the PSTN, its _**DP-robustness**_ under _NextFirst_.


---
# Using this tool

### Build
A simple `make` should do the trick: 

```
$ cd src/
$ make
$ cd ../
```

However you'll have to adapt the Makefile according to your own system. Oh, and you absolutely need C++11 or higher. 

## Encoding a PSTN
We will show how to encode the following temporal network:

![test_6c.json](/img/test_6c.png)

In fact, this PSTN gives a good overview of the capabilities of the tool, as it involves the five different types of probability distributions: [**uniform**](https://en.wikipedia.org/wiki/Discrete_uniform_distribution), [**normal**](https://en.wikipedia.org/wiki/Normal_distribution), [**PERT**](https://en.wikipedia.org/wiki/PERT_distribution), [**modified-Pert**](https://en.wikipedia.org/wiki/PERT_distribution), and **custom**.  


This is the corresponding (**JSON**) encoding accepted by the tool:

```json
{
	"Comment": "Test PSTN 6c",
	"Horizon": { "TimeUnit_nSeconds": 60, "TimeUnit_n": 40 },
	"TimeEvents": { 
		"t_00": { 	"TimeWindow": [0, "inf"] 	},
		"t_01": { 	"TimeWindow": [0, "inf"] 	},
		"t_02": { 	"TimeWindow": [0, "inf"] 	},
		"t_03": { 	"TimeWindow": [8, 10] 		},
		"t_04": { 	"TimeWindow": [0, "inf"] 	},
		"t_05": { 	"TimeWindow": [0, "inf"] 	},
		"t_06": { 	"TimeWindow": [0, "inf"] 	},
		"t_07": { 	"TimeWindow": [0, "inf"] 	},
		"t_08": { 	"TimeWindow": [0, "inf"] 	},
		"t_09": { 	"TimeWindow": [0, "inf"] 	}
	},
	"TemporalConstraints": {
		"c_0_1" : { "From": "t_00", "To": "t_01", "Type": "Uniform", 		"Params": { "Min": 0, "Max": 2}							},
		"c_1_2" : { "From": "t_01", "To": "t_02", "Type": "Gaussian", 		"Params": { "Mean": 5, "StdDev": 2}						},
		"c_2_3" : { "From": "t_02", "To": "t_03", "Type": "Controllable", 	"Params": { "LB": 4, "UB": 8}							},
		"c_1_4" : { "From": "t_01", "To": "t_04", "Type": "Controllable", 	"Params": { "LB": 0, "UB": "inf"}						},
		"c_4_5" : { "From": "t_04", "To": "t_05", "Type": "Controllable", 	"Params": { "LB": 2, "UB": "inf"}						},
		"c_5_6" : { "From": "t_05", "To": "t_06", "Type": "PERT", 			"Params": { "Min": 1, "Mode": 3, "Max": 9}				},
		"c_6_9" : { "From": "t_06", "To": "t_09", "Type": "Controllable", 	"Params": { "LB": 0, "UB": "inf"}						},
		"c_4_7" : { "From": "t_04", "To": "t_07", "Type": "mod-PERT", 		"Params": { "Min": 1, "Mode": 3, "Max": 9, "Skew": 0.5}	},
		"c_7_8" : { "From": "t_07", "To": "t_08", "Type": "Custom", 		"Distribution": "X"										},
		"c_8_9" : { "From": "t_08", "To": "t_09", "Type": "Controllable", 	"Params": { "LB": 0, "UB": "inf"}						},
		"c_4_9" : { "From": "t_04", "To": "t_09", "Type": "Controllable", 	"Params": { "LB": 0, "UB": 10}							},
		"c_9_3" : { "From": "t_09", "To": "t_03", "Type": "Controllable", 	"Params": { "LB": 2, "UB": "inf"}						}
	},
	"ProbabilityDistributions": {
		"X" : {  "2": 0.0015, "3": 0.02, "4": 0.17, "5": 0.25, "6": 0.19, "7": 0.14, "8": 0.1, "9": 0.07, "10": 0.04, "11": 0.01, "12": 0.005, "13": 0.0025, "14": 0.001 }
	}
}
```

The syntax is quite obvious.  
`TimeUnit_n` defines the size, in time units, of the discrete time horizon (the final deadline).  
`TimeUnit_nSeconds` tells the number of seconds that each time unit represents. It is not used by the tool, however for some reason it is not optional.

## Running the tool
`./bin/main -I benchmarks/test_6c.json` should yield:

```
PSTN: Test PSTN 6c	#TEs: 10	TU:40
Computing expected robustness...
% robustness: 7.48162
```

which means that the PSTN involves 10 time events, and runs on a horizon of 40 time units, and finally that its probability of success when executed under _NextFirst_ dispatching protocol is of exactly **0.0748162**. 

Yet, if you don't trust my algorithm (and further the maths in the paper), you may probably trust a Monte Carlo simulation (if you don't trust me, then you should probably code your own MC simulation) that simulates the _NextFirst_ execution, first with 1000 sampled scenarios:

```
PSTN: Test PSTN 6c	#TEs: 10	TU:40
Permoring Monte Carlo simulation (1000 samples)...
% success: 7.6
```

then with 1.000.000 samples:

```
PSTN: Test PSTN 6c	#TEs: 10	TU:40
Permoring Monte Carlo simulation (1000000 samples)...
% success: 7.4454
```


then with 100.000.000 samples (take a coffee):

```
PSTN: Test PSTN 6c	#TEs: 10	TU:40
Permoring Monte Carlo simulation (100000000 samples)...
% success: 7.48334
```

Now you can also play with the `--verbose+` option and visualize a summary of the network's structure with the _**input probabilities**_:

![input probabilities](/img/input.png)

and how each time event get _**probabilistically**_ (or experimentally, if you Monte Carlo `-M`) assigned a _**time value**_:

![output probabilities](/img/output1.png)
![output probabilities](/img/output2.png)

where you can see that _t8_ event as probability 0.1067 to get assigned time value 10, whereas it will not exceed 15 with probability 0.8692, and deduce that it has 0.6217 probability to fall between 10 and 15 included.


## Testing the algorithm on whole benchmarks

The `./bin/main --test`  takes a PSTN as input and directly compares its robustness computed value with a Monte Carlo simulation, for instance,  
`./bin/main --test -I benchmarks/2rovers.json`:

```
PSTN: Two-rovers example from paper: Strong and Dynamic Controllability Robustness in Probabilistic Temporal Networks	#TEs: 12	TU:700
Permoring Monte Carlo simulation (100000 samples)...
Computing expected robustness...
OK  MC: 0.80834, E = 0.809664	  relative diff: 0.001635

```

Namely, `./bin/test` simply avoid calling twice `main`, and potentially even a third time: if the relative difference between the computed value and the measured average is too high, then the Monte Carlo simulation will be rerun with a higher number of samples; only then, if there is still a too high difference, the test fails.

There a few simple scripts that use `./bin/test` to validate our algorithm on entire benchmarks:

* `./test_all.sh` : validate on all home made instances (_e.g._ test_6c showed above) ��these usually are small but tricky instances, constructed as the algorithm was under design;

* `./test_all_Akmal_uncontrollable.sh` : validate on all the uncontrollable part of the benchmark from [Akmal 2019](https://www.aaai.org/ojs/index.php/ICAPS/article/download/3456/3324), a set of 110 uncontrollable PSTN's;

* `./test_all_Akmal_controllable.sh` : idem with the remaining 452 controllable PSTN's from the same dataset;

* `./test_all_ordinary.sh` : here we consider Akmal's uncontrollable dataset, where we replaced all the initial Uniform distributions by ordinary ones we generated pseudo-randomly (further explained in our [paper](2020_PSTNs_with_Ordinary_Distributions_-_preprint.pdf)).

Note that in case of Akmal instances, the initial durations and probability distributions (which are all Uniform in this dataset!) are continuous. 
However our method requires discretized time. 
There is then a `-D` option for our `./bin/main` executable that define the number of decimals to be taken into account at rounding.
This is further explained in our [paper](2020_PSTNs_with_Ordinary_Distributions_-_preprint.pdf).


---

# C++ library  : _Integrating the algorithm within your own projects_

### Copyright: _GPLv3_

This is probably not the most interesting thing to read, however it is definitly the most important one to write: this library is under _GNU Public License version 3_.

>Copyright (C) 2019  Michael Saint-Guillain
>
>This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
>
>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
>
>You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

Basically, it means that anyone is allowed to reuse/adapt (parts of) this library, yet only for open source purposes; in other words, you are **_not_** allowed to make any *merchantile* use of it. 

### Why and how would you use this library

There are two possible reasons: 

1. **You're considering exploiting this library in a scientific research context.** In this case, your research subject is necessarily about PSTN's and thus, I am _potentially_ interested in collaborating. Contacting me is in fact most probably the more efficient/easy way to integrate and use this library in your project. 
2. **You need this library for a commercial project.** In this case, you should probably read again the paragraph above and consider finding a good lawyer. Feel free to write me in any case: _m dot stguillain at gmail dot com_.

## Basic usage: _hello PSTN_

Let's start with the most possibly basic use of our library; a kind of hello world of PSTN's:

**_Under writing_**

