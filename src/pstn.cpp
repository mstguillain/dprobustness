/*
Copyright 2019 Michael Saint-Guillain.
Contact: m.stguillain at gmail dot com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/



#include <sstream>
#include <iostream>
#include <vector>
#include <deque>
#include <map>
#include <cmath>

#include "pstn.h"
#include "prob.h"

#include "tools.h"
#include "json.hpp"

using json = nlohmann::json;
using namespace std;












PSTN_TimeEvent::PSTN_TimeEvent(string name, int id_, string comment) {
	this->name = name;
	this->id_ = id_;
	this->comment = comment;
}
PSTN_TimeEvent::~PSTN_TimeEvent() {
}

void PSTN_TimeEvent::setComment(string c) {
	comment = c;
}

void PSTN_TimeEvent::setTimeWindow(int lb, int ub) {
	ASSERT(lb >= 0, lb);
	ASSERT(ub >= 0, ub);
	ASSERT(lb <= ub, lb << " > " << ub);

	this->lb = lb;
	this->ub = ub;
}

/* add a controllable temporal constraint */
PSTN_TemporalConstraint& PSTN_TimeEvent::addConstraint(string name, const PSTN_TimeEvent& with_e, int lb, int ub, string comment) {
	PSTN_TemporalConstraint* c = new PSTN_TemporalConstraint(name, with_e, lb, ub, comment);
	constraints.push_back(c);

	if(this->type == CONTINGENT)
		this->type = MIXED;

	return *c;
}

/* add a probabilistic temporal constraint */
PSTN_TemporalConstraint& PSTN_TimeEvent::addConstraint(string name, const PSTN_TimeEvent& with_e, const PSTN_PrDistribution& x, string comment) {
	PSTN_TemporalConstraint* c = new PSTN_TemporalConstraint(name, with_e, x, comment);
	constraints.push_back(c);

	if(this->type == CONTROLLABLE and constraints.size() >= 2)
		this->type = MIXED;
	else
		this->type = CONTINGENT;

	return *c;
}


void PSTN_TimeEvent::removeConstraint(PSTN_TemporalConstraint& c) {
	constraints.erase(remove(constraints.begin(), constraints.end(), &c), constraints.end());

	delete &c;
}


bool PSTN_TimeEvent::feasibleTime(int t, const map<const PSTN_TimeEvent*, int> & timeEventValue) const {
	bool success = lb <= t and t <= ub;								// check time window
	for (const PSTN_TemporalConstraint* c: getConstraints()) {		// check temporal constraints with dependencies
		if (c->getType() == CONTINGENT) continue;
		if (not success) break;
		double t_dep = timeEventValue.at(c->getTimeEventDependence());
		success = success and t_dep + c->getLB() <= t and t <= t_dep + c->getUB();
	}
	return success;
}












PSTN_TemporalConstraint::PSTN_TemporalConstraint(string name, const PSTN_TimeEvent& with_e, int lb, int ub, string comment) {
	_ASSERT_(lb >= 0, lb);
	_ASSERT_(ub >= 0, ub);
	_ASSERT_(lb <= ub, lb << " > " << ub);

	this->type = CONTROLLABLE; 

	this->name 		= name;
	this->with_e 	= &with_e;
	this->lb 		= lb;
	this->ub 		= ub;
	this->comment 	= comment;
}
PSTN_TemporalConstraint::PSTN_TemporalConstraint(string name, const PSTN_TimeEvent& with_e, const PSTN_PrDistribution& x, string comment) {
	this->type = CONTINGENT; 

	this->name 		= name;
	this->with_e 	= & with_e;
	this->prob_dist = & x;
	this->comment 	= comment;
	this->lb 		= 0;
	this->ub 		= numeric_limits<int>::max();
}
PSTN_TemporalConstraint::~PSTN_TemporalConstraint() {
	if (prob_dist != nullptr)
		delete prob_dist;
}


int PSTN_TemporalConstraint::getLB() const { 
	if (type == CONTROLLABLE) {
		// ASSERT(0 <= lb and lb <= 10000000, lb);
		return lb;
	}
	else {
		return prob_dist->t_min();
	}
}
int PSTN_TemporalConstraint::getUB() const { 
	if (type == CONTROLLABLE) {
		// ASSERT(0 <= ub and ub <= 10000000, ub);
		return ub;
	}
	else {
		return prob_dist->t_max();
	}
}

const PSTN_TimeEvent* PSTN_TemporalConstraint::getTimeEventDependence() const {
	ASSERT(with_e != nullptr, "");
	return with_e;
}

int PSTN_TemporalConstraint::sample() const {
	ASSERT(type != CONTROLLABLE, "");
	return prob_dist->sample();
}
const string& PSTN_TemporalConstraint::drawSampledPDF(int n) const {
	ASSERT(type != CONTROLLABLE, "");
	return prob_dist->drawSampledPDF(n);
}

const double* PSTN_TemporalConstraint::getPDF() const {
	ASSERT(type != CONTROLLABLE, "");
	return prob_dist->getPDF();
}

const string& PSTN_TemporalConstraint::printPDF() const {
	ASSERT(type != CONTROLLABLE, "");
	return prob_dist->printPDF();
}
const string& PSTN_TemporalConstraint::printCDF() const {
	ASSERT(type != CONTROLLABLE, "");
	return prob_dist->printCDF();
}



double PSTN_TemporalConstraint::getProbDuration(int t) const {
	// ASSERT(0 <= t and t <= n_time_units, t);
	if (type == CONTROLLABLE)
		return 1.0 * (t == lb);
	
	ASSERT(prob_dist != nullptr, "");
	return prob_dist->p(t);
}


















PSTN_Network::PSTN_Network() {
	// REMINDER: there is no time event at the moment this costructor is called!
}

PSTN_Network::~PSTN_Network() {
	for (PSTN_TimeEvent* te: time_events) {
		// delete & next_time_events.at(te);  --> no need! "The rule is that when you clear a vector of objects, the destructor of each element will be called. On the other hand, if you have a vector of pointers, vector::clear() will not call delete on them, and you have to delete them yourself."
		// delete & prev_time_events.at(te);	idem
		// delete & ancestors.at(te);			idem
		
		delete timeEventProbas[te->id()];
		delete te;
	}
	for (PSTN_TemporalConstraint* c: time_constraints)
		delete c;

}

PSTN_TimeEvent& PSTN_Network::addTimeEvent(string name, string comment) {
	_ASSERT_(n_time_units > 0, "Object not initialized!");
	_ASSERT_(time_events.size() < MAX_TE-1, "");

	PSTN_TimeEvent *e = new PSTN_TimeEvent(name, time_events.size(), comment);
	time_events.push_back(e);

	// timeEventProbas[e] = new double[n_time_units + 1];
	// fill_n(timeEventProbas.at(e), n_time_units + 1, -1.0);
	
	// timeEventProbas[e] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
	timeEventProbas[e->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);

	next_time_events[e] = * new vector<const PSTN_TimeEvent*>;
	prev_time_events[e] = * new vector<const PSTN_TimeEvent*>;
	ancestors[e] = * new set<const PSTN_TimeEvent*>;
	ancestors_with_depth[e] = * new set<pair<const PSTN_TimeEvent*, int>>;
	
	return *e;
}


PSTN_TemporalConstraint& PSTN_Network::addTemporalConstraint(string name, PSTN_TimeEvent& to_te, const PSTN_TimeEvent& with_te, int lb, int ub, string comment) {
	_ASSERT_(n_time_units > 0, "Object not initialized!");
	_ASSERT_(find(prev_time_events.at(&to_te).begin(), prev_time_events.at(&to_te).end(), &with_te) == prev_time_events.at(&to_te).end(), "trying to add two dependencies from " << with_te << " to " << to_te);

	PSTN_TemporalConstraint *c = & to_te.addConstraint(name, with_te, lb, ub, comment);
	time_constraints.push_back(c);
	return *c;
}

PSTN_TemporalConstraint& PSTN_Network::addTemporalConstraint(string name, PSTN_TimeEvent& to_te, const PSTN_TimeEvent& with_te, const PSTN_PrDistribution& x, string comment) {
	_ASSERT_(n_time_units > 0, "Object not initialized!");
	_ASSERT_(find(prev_time_events.at(&to_te).begin(), prev_time_events.at(&to_te).end(), &with_te) == prev_time_events.at(&to_te).end(), "trying to add two dependencies from " << with_te << " to " << to_te);

	PSTN_TemporalConstraint *c = & to_te.addConstraint(name, with_te, x, comment);
	time_constraints.push_back(c);
	return *c;
}

PSTN_TemporalConstraint& PSTN_Network::getTemporalConstraint(string name) {
	_ASSERT_(n_time_units > 0, "Object not initialized!");

	for (PSTN_TemporalConstraint* c: time_constraints)
		if (c->getName() == name)
			return *c;

	_ASSERT_(false, "Temporal constraint " << name << " not found!");
}


PSTN_TimeEvent& PSTN_Network::getTimeEvent(string name) {
	_ASSERT_(n_time_units > 0, "Object not initialized!");

	for (PSTN_TimeEvent* te: time_events)
		if (te->getName() == name)
			return *te;

	_ASSERT_(false, "Time event " << name << " not found!");
}











































void PSTN_Network::findDependencies(int verbose) {
	// TODO: add cycle detection (use done variable)


	if (verbose) cout << "Determine network structure ... " << endl << "\tFind root and leaf nodes" << endl << flush; 
	// compute network structure: find root nodes, set up "next" variables
	for (const PSTN_TimeEvent* te: getTimeEvents()) {
		next_time_events.at(te).clear();
		prev_time_events.at(te).clear();
	}
	root_time_events.clear();
	for (const PSTN_TimeEvent* te: getTimeEvents()) {
		if (te->getConstraints().size() == 0)
			root_time_events.push_back(te);
		else 
			for (const PSTN_TemporalConstraint* c : te->getConstraints()) {
				next_time_events.at(c->getTimeEventDependence()).push_back(te);
				prev_time_events.at(te).push_back(c->getTimeEventDependence());
				constraint[make_pair(c->getTimeEventDependence(), te)] = c;
			}
	}
	// find the leaf node and check it is unique
	for (const PSTN_TimeEvent* te: getTimeEvents())
		if (next_time_events.at(te).size() == 0)
			leaf_time_events.push_back(te);
	
	if (leaf_time_events.size() != 1) {

		PSTN_TimeEvent &t_final = addTimeEvent("t_F", "Final TE (added because >1 leaf TE)");
		for (const PSTN_TimeEvent* te: leaf_time_events) {
			const PSTN_TemporalConstraint& c = addTemporalConstraint("c_" + te->getName() + "_F", t_final, *te, 0, numeric_limits<int>::max());
			next_time_events.at(te).push_back(&t_final);
			prev_time_events.at(&t_final).push_back(te);
			constraint[make_pair(te, &t_final)] = &c;
		}
		// cout << "Error! # leaf time events: " << leaf_time_events.size() << endl;
		// for (const PSTN_TimeEvent* te: leaf_time_events)
		// 	cout << *te << endl;
		// _ASSERT_(false, "");
		leaf_time_events.clear(); leaf_time_events.push_back(&t_final);
	}


	if (verbose) cout << "\tCompute ancestor sets" << endl << flush; 
	// fill ancestors
	distance.clear();
	for (const PSTN_TimeEvent* te: getTimeEvents()) {
		ancestors.at(te).clear();
		ancestors_with_depth.at(te).clear();
	}
	for (const PSTN_TimeEvent* te: getTimeEvents())
		rec_ancestors(te, te, 0);	

	if (verbose) cout << "\tDetermine predictability" << endl << flush; 
	// determine whether each event is predicatble or not
	predictable.clear();
	for (const PSTN_TimeEvent* leaf_te: leaf_time_events)
		rec_predict(leaf_te);
	if (verbose >= 3)
		for (const PSTN_TimeEvent* te: getTimeEvents())
			if (not predictable.at(te))
				cout << *te << " is not predictable" << endl;


	if (verbose) cout << "\tDetermine dependencies" << endl << flush; 
	// determine dependencies: two events (t_i, t_j) may be dependent as soon as they have a common unpredictable ancestor
	//													OR if one is an ancestor of the other
	dependent.clear();
	for (const PSTN_TimeEvent* te_i: getTimeEvents()) {
		for (const PSTN_TimeEvent* te_h: getTimeEvents()) {

			if (te_i == te_h or dependent.find(make_pair(te_i, te_h)) != dependent.end()) // skip if already done
				continue;

			if (ancestors.at(te_i).find(te_h) != ancestors.at(te_i).end() or ancestors.at(te_h).find(te_i) != ancestors.at(te_h).end()) {
				dependent[make_pair(te_i, te_h)] = true;
				dependent[make_pair(te_h, te_i)] = true;
				continue;
			}

			if (verbose >= 3) { 
				cout << "ancestors of " << *te_i << ": "; 
				for (const PSTN_TimeEvent* te_a: ancestors.at(te_i))
					cout << *te_a << ", ";
				cout << endl << "ancestors of " << *te_h << ": "; 
				for (const PSTN_TimeEvent* te_a: ancestors.at(te_h))
					cout << *te_a << ", ";
				cout << endl << "\tinsertection: " << flush;
			}

			vector<const PSTN_TimeEvent*> v_intersection;				// compute intersection between the two ancestor sets
			set_intersection(ancestors.at(te_i).begin(), ancestors.at(te_i).end(),
							ancestors.at(te_h).begin(), ancestors.at(te_h).end(),
							std::back_inserter(v_intersection));

			bool independent = true;								// all the events in the intersection must be predictable
			for (const PSTN_TimeEvent* te_a: v_intersection) {		// for (te_i, te_h) to be independent
				independent = independent and predictable.at(te_a);
				if (verbose >= 3) cout << *te_a << ",";
			}
			
			dependent[make_pair(te_i, te_h)] = not independent;
			dependent[make_pair(te_h, te_i)] = not independent;

			if (verbose >= 3 and dependent[make_pair(te_i, te_h)]) 
				cout << "\t\t" << *te_i << "," << *te_h << " are dependent!" << endl; 
			if (verbose >= 3) cout << endl << endl << flush;
				
		}
	}
}

bool PSTN_Network::rec_predict(const PSTN_TimeEvent* curr_te) {
	bool predict = true;

	for (const PSTN_TemporalConstraint* c : curr_te->getConstraints()) 
		predict = rec_predict(c->getTimeEventDependence()) and c->getType() == CONTROLLABLE and predict;

	predictable[curr_te] = predict;
	return predict;
}

void PSTN_Network::rec_ancestors(const PSTN_TimeEvent* curr_te, const PSTN_TimeEvent* start_te, int depth) {
	if (curr_te != start_te)
		ancestors.at(start_te).insert(curr_te);
	ancestors_with_depth.at(start_te).insert(make_pair(curr_te, depth));
	distance[make_pair(start_te, curr_te)] = depth;
	distance[make_pair(curr_te, start_te)] = depth;
	for (const PSTN_TimeEvent* te: prev_time_events.at(curr_te))
		rec_ancestors(te, start_te, depth+1);
}


/* TODO : IMPROVE by computing the longer timing path between events : 
	improve the 	c__->getLB() == 0
	as well as 		c__->getUB() >= n_time_units
*/
void PSTN_Network::spotExtraConstraints(bool verbose) {
	verbose = false;
	vector<const PSTN_TemporalConstraint *> to_remove;
	for (const PSTN_TimeEvent* te: getTimeEvents()) {
		for (const PSTN_TimeEvent* prev_te: prev_time_events.at(te)) {
			for (const PSTN_TimeEvent* prev_te__: prev_time_events.at(te)) {
				if (prev_te__ == prev_te) continue;
				const PSTN_TemporalConstraint* c__ = constraint.at(make_pair(prev_te__, te));
				if (ancestors.at(prev_te).find(prev_te__) !=  ancestors.at(prev_te).end()
							and c__->getLB() <= 0 and c__->getUB() >= n_time_units) {
					skip_constraint.insert(make_pair(prev_te__, te));
					// cout << *te << ": skip constraint " << *prev_te__ << " -> " << *te << endl;
				} 
			}
		}
	}
}



















Results PSTN_Network::DProbustness(int verbose) {
	_ASSERT_(not time_events.empty(), "Empty network! ");

	findDependencies(verbose);
	spotExtraConstraints(verbose);
	if (verbose) cout << "Start computation ..." << flush; 

	map<const PSTN_TimeEvent*, bool> done;
	for (const PSTN_TimeEvent* te: getTimeEvents()) {
		timeEventProbas[te->id()]->clear();
		done[te] = false;
	}

	deque<const PSTN_TimeEvent*> timeEventsToDO;
	for (const PSTN_TimeEvent* te: getTimeEvents())
		timeEventsToDO.push_back(te);

	int i = 0;	// cycle detection
	const PSTN_TimeEvent* t_leaf = nullptr;
	while (not timeEventsToDO.empty()) {
		if (i > (int) timeEventsToDO.size())
			_ASSERT_(false, "Cycle detected!");

		const PSTN_TimeEvent &te = *(timeEventsToDO.front()); timeEventsToDO.pop_front();
		
		bool dep_done = true;
		for (const PSTN_TimeEvent* te_prev: prev_time_events.at(&te))
			dep_done = dep_done and done.at(te_prev);

		if (not dep_done) {
			timeEventsToDO.push_back(&te);
			i++;
		}
		else {	
			i = 0;
			if (verbose == 1) cout << endl << setw(15) << te.getName() << flush;
			if (verbose >= 2) cout << endl << te << flush;
			computeProbasTimeEvent(te, timeEventProbas);
			if (verbose >= 1) cout << " \t" << timeEventProbas[te.id()]->F(n_time_units);
			if (verbose >= 2) cout << endl << timeEventProbas[te.id()] << endl;

			done[&te] = true;
			if (timeEventsToDO.empty())
				t_leaf = &te;
		}
	}
	
	Results res;
	for (const PSTN_TimeEvent* te: getTimeEvents())
		for (int t = 0; t <= n_time_units; t++) 
			res.time_probas[te][t] = timeEventProbas[te->id()]->p(t);

	res.robustness = 0.0;
	for (int t = 0; t <= n_time_units; t++) 
		res.robustness += timeEventProbas[t_leaf->id()]->p(t);


	if (verbose) cout << " Done." << endl << flush; 
	return res;
}

















void PSTN_Network::computeProbasTimeEvent(const PSTN_TimeEvent 	&te, PSTN_PrDistribution **timeProbas) {

	int lb = max(0, te.getTW_lb()); 
	int ub = min(n_time_units, te.getTW_ub());

	int t_min_prev = 0, t_max_prev = 0;
	// int start;

	for(const PSTN_TemporalConstraint *c : te.getConstraints()) {
		t_min_prev = max(t_min_prev, timeProbas[c->getTimeEventDependence()->id()]->t_min() + c->getLB());
		if (c->getType() == CONTROLLABLE)
			t_max_prev = max(t_max_prev, timeProbas[c->getTimeEventDependence()->id()]->t_max() + c->getLB());
		else
			t_max_prev = max(t_max_prev, timeProbas[c->getTimeEventDependence()->id()]->t_max() + c->getUB());
	}
	ub = min(t_max_prev, te.getTW_ub());
	int start = t_min_prev > lb ? t_min_prev : lb +1;

	switch (te.getConstraints().size()) {
		case 0:
			timeProbas[te.id()]->p(lb, 1.0);
			break;

		case 1:
			


			for (int t = t_min_prev; t <= lb; t++) 						//   t = lb
				timeProbas[te.id()]->p_add(lb, f(te, t, timeProbas));
			for (int t = start; t <= ub; t++) 							//   t > lb
				timeProbas[te.id()]->p(t, f(te, t, timeProbas));

			break;
		
		default:

			if (te.getName() == "t_F" and skip_t_F)
				break;			

			map<int, PSTN_PrDistribution** > timeProbas_fixed;

			// if (te.getName() == "t_F") {
			// 	timeProbas[te.id()]->p(n_time_units, g_F(te, timeProbas, timeProbas_fixed));
			// } 
			// else {
				for (int t = t_min_prev; t <= lb ; t++) 					//   t = lb
					timeProbas[te.id()]->p_add(lb, g(te, t, timeProbas, timeProbas_fixed));
				for (int t = start; t <= ub; t++) 							//   t > lb
					timeProbas[te.id()]->p(t, g(te, t, timeProbas, timeProbas_fixed));
			// }

			break;
	}
}
















/* called only if we don't need the final time event distribution, i.e. if we only need its success prob */
double PSTN_Network::g_F(const PSTN_TimeEvent &te, PSTN_PrDistribution** timeProbas, map<int, PSTN_PrDistribution**> &timeProbas_fixed) {
	cout << "g_F called... ";

	/* Find the set of dependent variables */
	set<const PSTN_TimeEvent*> dep_events;
	for (const PSTN_TimeEvent* e_1 : prev_time_events.at(&te))
		for (const PSTN_TimeEvent* e_2 : prev_time_events.at(&te))
			if (e_1 != e_2 and dependent.at(make_pair(e_1, e_2))) {
				dep_events.insert(e_1);
				dep_events.insert(e_2);
			}


	double p_success_independent = 1.0;
	for (const PSTN_TimeEvent* e_ : prev_time_events.at(&te))
		if (dep_events.find(e_) == dep_events.end())
			p_success_independent *= timeProbas[e_->id()]->F(n_time_units);



	vector<const PSTN_TimeEvent*> e; 
	vector<int> l, u;
	for (const PSTN_TimeEvent* prev_te: dep_events) {
		const PSTN_TemporalConstraint* c = constraint.at(make_pair(prev_te, &te));
		if(skip_constraint.find(make_pair(prev_te, &te)) == skip_constraint.end()) {
			e.push_back(c->getTimeEventDependence());
			l.push_back(c->getLB());	
			u.push_back(min(n_time_units+1, c->getUB()));	
		}
	} 

	if (e.size() == 0)
		return p_success_independent;
	if (e.size() == 1)
		return p_success_independent * timeProbas[e.at(0)->id()]->F(n_time_units);

	cout << "calling max_timeEvents with e: ";
	for (const PSTN_TimeEvent* e_: e)
		cout << *e_ << ", ";
	cout << endl;

	double p_ = 0.0;
	for (int t = 0; t <= n_time_units ; t++) 				
		p_ += max_timeEvents(e, l, u, t, timeProbas, timeProbas_fixed);
		
	return p_success_independent * p_;

}
				

double PSTN_Network::f(const PSTN_TimeEvent &te, int t, PSTN_PrDistribution** timeProbas) {
	ASSERT(te.getConstraints().size() == 1, te);
	ASSERT(0 <= t and t <= n_time_units, t);

	// cout << "f(" << t << ") called for " << te.getName() << endl << flush;
	
	const PSTN_TemporalConstraint 	&c = * te.getConstraints().front();	
	const PSTN_PrDistribution 		&pr_with_e = *timeProbas[c.getTimeEventDependence()->id()];

	if (t - c.getLB() < 0 or t < pr_with_e.t_min())
		return 0.0;
	

	if (c.getType() == CONTROLLABLE)
		return pr_with_e.p(t - c.getLB());

	if (t > pr_with_e.t_max() + c.getUB())
		return 0.0;
	

	double p = 0.0;
	for (int d = max(t - pr_with_e.t_max(), c.getLB()); d <= min(t, c.getUB()); d++) 
		p += c.getProbDuration(d) * pr_with_e.p(t - d);
	// if (p <= ZERO) return 0.0;
	return p;
}


double PSTN_Network::g(const PSTN_TimeEvent &te, int t, PSTN_PrDistribution** timeProbas, map<int, PSTN_PrDistribution**> &timeProbas_fixed) {
	_ASSERT_(te.getType() != MIXED, te);
	_ASSERT_(te.getType() != CONTINGENT, te);
	ASSERT(0 <= t and t <= n_time_units, t);

	// cout << endl << "g(" << t << ") called for " << te.getName() << endl << flush;
	
	
	for (const PSTN_TemporalConstraint* c : te.getConstraints())
		if (t - c->getLB() < 0)
			return 0.0;

	for (const PSTN_TemporalConstraint* c : te.getConstraints())
		if (t - c->getLB() < timeProbas[c->getTimeEventDependence()->id()]->t_min()) 
			return 0.0;

	bool too_late = true;
	for (const PSTN_TemporalConstraint* c : te.getConstraints())
		too_late = too_late and t - c->getLB() > timeProbas[c->getTimeEventDependence()->id()]->t_max();
	if (too_late)	
		return 0.0;


	vector<const PSTN_TimeEvent*> e; 
	vector<int> l, u;

	for (const PSTN_TimeEvent* prev_te: prev_time_events.at(&te)) {
		const PSTN_TemporalConstraint* c = constraint.at(make_pair(prev_te, &te));
		if(skip_constraint.find(make_pair(prev_te, &te)) == skip_constraint.end()) {
			e.push_back(c->getTimeEventDependence());
			l.push_back(c->getLB());	
			u.push_back(min(n_time_units+1, c->getUB()));	
		}
	} 


	ASSERT(e.size() > 0, "");	
	if (e.size() == 1)	{
		const PSTN_TemporalConstraint* c = constraint.at(make_pair(e.at(0), &te));
		return timeProbas[e.at(0)->id()]->p(t - c->getLB());
	}

	return max_timeEvents(e, l, u, t, timeProbas, timeProbas_fixed);
}




















double PSTN_Network::max_timeEvents(vector<const PSTN_TimeEvent*> &e, vector<int> &l, vector<int> &u, int t, PSTN_PrDistribution **timeProbas, map<int, PSTN_PrDistribution**> &timeProbas_fixed) {
	ASSERT(0 <= t and t <= n_time_units, t);


	/* Identify time points with upper bound constraint u */
	vector<int> id_with_u, id_no_u;
	// vector<int> u_with_u, 
	vector<int> l_with_u, l_no_u;
	for (int i = 0; i < (int) e.size(); i++)
		if (u.at(i) < n_time_units) {
			id_with_u.push_back(e.at(i)->id());
			// u_with_u.push_back(u.at(i));
			l_with_u.push_back(l.at(i));
		} else {
			id_no_u.push_back(e.at(i)->id());
			l_no_u.push_back(l.at(i));
		}



	/* If random variables are independent, return simply the product */
	set<const PSTN_TimeEvent*> dep_events;
	for (const PSTN_TimeEvent* e_1 : e)
		for (const PSTN_TimeEvent* e_2 : e)
			if (e_1 != e_2 and dependent.at(make_pair(e_1, e_2))) {
				dep_events.insert(e_1);
				dep_events.insert(e_2);
			}


	if (dep_events.size() == 0) {

		double p_alpha = 1.0, p_beta = 1.0, p_a_or_b = 1.0, p_one = 1.0;
		double F_t_u_1[MAX_TE], p_u_l[MAX_TE], p___[MAX_TE];
		for (int i = 0; i < (int) e.size(); i++) {
			int e_id = e.at(i)->id();
			const PSTN_PrDistribution &pr = *timeProbas[e_id];
			F_t_u_1[e_id] = 0.0, p_u_l[e_id] = 0.0, p___[e_id] = 0.0;
			for (int x = pr.t_min(); x < t -u.at(i); x++) 
				F_t_u_1[e_id] += pr.p(x);
			for (int x = max(pr.t_min(), t -u.at(i)); x <= min(pr.t_max(), t -l.at(i)); x++) 
				p_u_l[e_id] += pr.p(x);
			for (int x = t -l.at(i) +1; x <= pr.t_max(); x++) 
				p___[e_id] += pr.p(x);
			p_alpha *= p_u_l[e_id];
			p_beta  *= F_t_u_1[e_id] + p_u_l[e_id] - pr.p(t -l.at(i));
			p_one 	*= F_t_u_1[e_id] + p_u_l[e_id] + p___[e_id];
		}
		p_beta = p_one - p_beta;
		

		p_a_or_b = 0.0;
		int sign = 1;
		for (int size = 1; size <= (int) id_with_u.size(); size++) {				// FOR EACH SIZE OF SUBSET
			vector<unsigned int> subsets = generate_subsets(id_with_u.size(), size);
			for (unsigned int s : subsets) {								// FOR EACH SUBSET OF FIXED SIZE
				double A = 1.0;
				for (int i = 0; i < (int) min(id_with_u.size(), sizeof(unsigned int) * 8); i++) {	// FOR each element of the subset
					if ((1 << i) & s) {
						A *= F_t_u_1[id_with_u.at(i)];
						if (A <= ZERO) break;
					} else {
						A *= F_t_u_1[id_with_u.at(i)] + p_u_l[id_with_u.at(i)] - timeProbas[id_with_u.at(i)]->p(t -l_with_u.at(i));
					}
				}
				for (int i = 0; i < (int) id_no_u.size(); i++) {
					if (A <= ZERO) break;
					A *= F_t_u_1[id_no_u.at(i)] + p_u_l[id_no_u.at(i)] - timeProbas[id_no_u.at(i)]->p(t -l_no_u.at(i));
				}
				p_a_or_b += sign * A;
			} 
			sign *= -1;
		}
		p_a_or_b = p_one - p_a_or_b;


		double p = p_alpha + p_beta - p_a_or_b;
		if (p <= ZERO) p = 0.0;
		return p;
	}




	/* OTHERWISE: make them independent by using total probability theorem on the closest common ancestor */

	// compute the intersection of the ancestor(+e_i) sets, of dependent events only


	vector<const PSTN_TimeEvent*> vec_dep_events;
	for (const PSTN_TimeEvent* e_ : dep_events)
		vec_dep_events.push_back(e_);

	set<const PSTN_TimeEvent*> intersection;
	for (const PSTN_TimeEvent* a : ancestors.at(vec_dep_events.at(0))) 
		intersection.insert(a);
		
	
	intersection.insert(vec_dep_events.at(0));
	for (int i = 1; i < (int) vec_dep_events.size(); i++) {
		set<const PSTN_TimeEvent*> set_1;
		for (const PSTN_TimeEvent* e_ : intersection)
			set_1.insert(e_);

		set<const PSTN_TimeEvent*> set_2 = ancestors.at(vec_dep_events.at(i));;
		set_2.insert(vec_dep_events.at(i));

		intersection.clear();
		set_intersection(set_1.begin(), set_1.end(),
						 set_2.begin(), set_2.end(),
						std::inserter(intersection, intersection.begin()));
	}
	
	
	// cout << "e: ";
	// for (const PSTN_TimeEvent* e_i : e)
	// 	cout << e_i->getName() << ", ";
	// cout << endl;	

	// cout << "intersection: ";
	// for (const PSTN_TimeEvent* a : intersection)
	// 	cout << a->getName() << " , ";
	// cout << endl;	


	/* the closest common ancestor is the one that has a minimum distance with one of the e_i's */
	const PSTN_TimeEvent* closest_a = nullptr;
	
	int min_depth = numeric_limits<int>::max();
	int n_closest = 0;
	for (const PSTN_TimeEvent* a : intersection) {
		int dist = numeric_limits<int>::max();
		for (const PSTN_TimeEvent* e_i : dep_events) {
			int d = distance.at(make_pair(a, e_i));
			if (d < dist)
				dist = d;
		}
		if (dist == min_depth) 
			n_closest++;	
		if (dist < min_depth) {
			min_depth = dist;
			closest_a = a;
			n_closest =1;
		}
	}
	_ASSERT_(n_closest == 1, "#### ERROR: more than one closest ancestor!  " << n_closest);
	





	deque<const PSTN_TimeEvent*> timeEventsToDO;
	bool done[MAX_TE];

 	bool timeProbas_fixed_already_computed = true;
	if (timeProbas_fixed.find(timeProbas[closest_a->id()]->t_min()) == timeProbas_fixed.end()) {

		// cout << "closest ancestor: " << closest_a->getName() << " min: " << timeProbas[closest_a->id()]->t_min() << "  max: " << timeProbas[closest_a->id()]->t_max() << endl << endl;
		// cout << "inter-dependent events: ";
		// for (const PSTN_TimeEvent* e_i : vec_dep_events)
		// 	cout << *e_i << ", ";
		// cout << endl;


		timeProbas_fixed_already_computed = false;


		for (int t_fixed = timeProbas[closest_a->id()]->t_min(); t_fixed <= timeProbas[closest_a->id()]->t_max(); t_fixed++) {
			timeProbas_fixed[t_fixed] = new PSTN_PrDistribution*[getTimeEvents().size()+1];

			for (const PSTN_TimeEvent* te: getTimeEvents())
				timeProbas_fixed.at(t_fixed)[te->id()] = nullptr;

			for (const PSTN_TimeEvent* e_i : vec_dep_events)
				timeProbas_fixed.at(t_fixed)[e_i->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
			
			if (timeProbas_fixed.at(t_fixed)[closest_a->id()] == nullptr)
				timeProbas_fixed.at(t_fixed)[closest_a->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
		}

		// new ---
		set<const PSTN_TimeEvent*> unshared_ancestors;
		for (const PSTN_TimeEvent* e_i : vec_dep_events)
			for (const PSTN_TimeEvent* a : ancestors.at(e_i))
				if (intersection.find(a) == intersection.end())	// if not in intersection
					unshared_ancestors.insert(a);

		// cout << "unshared_ancestors: ";
		// for (const PSTN_TimeEvent* e_ : unshared_ancestors)
		// 	cout << *e_ << ", ";
		// cout << endl;

		for (int t_fixed = timeProbas[closest_a->id()]->t_min(); t_fixed <= timeProbas[closest_a->id()]->t_max(); t_fixed++) 
			for (const PSTN_TimeEvent* a : unshared_ancestors)
				timeProbas_fixed.at(t_fixed)[a->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
		

		// fill the done indicator map with these time events only
		for (const PSTN_TimeEvent* te: getTimeEvents()) {
			if (timeProbas_fixed.at(timeProbas[closest_a->id()]->t_min())[te->id()] != nullptr) {
				timeEventsToDO.push_back(te);
				done[te->id()] = false;
			}
			else {
				for (int t_fixed = timeProbas[closest_a->id()]->t_min(); t_fixed <= timeProbas[closest_a->id()]->t_max(); t_fixed++)
					timeProbas_fixed.at(t_fixed)[te->id()] = timeProbas[te->id()];
				done[te->id()] = true;
			}
		}

	}

	// cout << "not done: ";
	// for (const PSTN_TimeEvent* te: getTimeEvents())
	// 	if (not done[te->id()])
	// 		cout << *te << ", ";
	// cout << endl;

	// cout << "time events to do: ";
	// for (const PSTN_TimeEvent* te: timeEventsToDO)
	// 	cout << *te << ", ";
	// cout << endl;


	double p = 0.0;
	for (int t_fixed = timeProbas[closest_a->id()]->t_min(); t_fixed <= timeProbas[closest_a->id()]->t_max(); t_fixed++) {	
		if (timeProbas[closest_a->id()]->p(t_fixed) <= ZERO) continue;

		if (not timeProbas_fixed_already_computed) {
		
			// cout << "computing with timeProbas_fixed " << *closest_a << ": t = " << t_fixed << endl << flush;

			timeProbas_fixed.at(t_fixed)[closest_a->id()]->p(t_fixed, 1.0);
			done[closest_a->id()] = true;

			int i = 0;
			bool finished = false;
			while (not finished) {
				_ASSERT_(i <= (int) timeEventsToDO.size(), "Cycle detected!");

				const PSTN_TimeEvent &te = *(timeEventsToDO.front()); 
				timeEventsToDO.pop_front(); 
				timeEventsToDO.push_back(&te);

				if (done[te.id()]) continue;

				bool dep_done = true;
				for (const PSTN_TimeEvent* te_prev: prev_time_events.at(&te))
					dep_done = dep_done and done[te_prev->id()];

				if (dep_done) {	
					ASSERT(timeProbas_fixed.at(t_fixed)[te.id()]->empty(), te);
					computeProbasTimeEvent(te, timeProbas_fixed.at(t_fixed));
				
					done[te.id()] = true;
					i = 0;

					finished = true;
					for (const PSTN_TimeEvent *te : timeEventsToDO)
						finished = finished and done[te->id()];
				}
				i++;
			}

		}

		double p_alpha = 1.0, p_beta = 1.0, p_one = 1.0;
		double F_t_u_1[MAX_TE], p_u_l[MAX_TE], p___[MAX_TE];
		for (int i = 0; i < (int) e.size(); i++) {
			int e_id = e.at(i)->id();
			const PSTN_PrDistribution &pr = *timeProbas_fixed.at(t_fixed)[e_id];
			F_t_u_1[e_id] = 0.0, p_u_l[e_id] = 0.0, p___[e_id] = 0.0;
			for (int x = pr.t_min(); x < t -u.at(i); x++) 
				F_t_u_1[e_id] += pr.p(x);
			for (int x = max(pr.t_min(), t -u.at(i)); x <= min(pr.t_max(), t -l.at(i)); x++) 
				p_u_l[e_id] += pr.p(x);
			for (int x = t -l.at(i) +1; x <= pr.t_max(); x++) 
				p___[e_id] += pr.p(x);
			p_alpha *= p_u_l[e_id];
			p_beta  *= F_t_u_1[e_id] + p_u_l[e_id] - pr.p(t -l.at(i));
			p_one 	*= F_t_u_1[e_id] + p_u_l[e_id] + p___[e_id];
		}
		p_beta = p_one - p_beta;


		double p_a_or_b = 0.0;
		int sign = 1;
		for (int size = 1; size <= (int) id_with_u.size(); size++) {				// FOR EACH SIZE OF SUBSET
			vector<unsigned int> subsets = generate_subsets(id_with_u.size(), size);
			for (unsigned int s : subsets) {								// FOR EACH SUBSET OF FIXED SIZE
				double A = 1.0;
				for (int i = 0; i < (int) min(id_with_u.size(), sizeof(unsigned int) * 8); i++) {	// FOR each element of the subset
					if ((1 << i) & s) {
						A *= F_t_u_1[id_with_u.at(i)];
						if (A <= ZERO) break;
					} else {
						A *= F_t_u_1[id_with_u.at(i)] + p_u_l[id_with_u.at(i)] - timeProbas_fixed.at(t_fixed)[id_with_u.at(i)]->p(t -l_with_u.at(i));
					}
				}
				for (int i = 0; i < (int) id_no_u.size(); i++) {
					if (A <= ZERO) break;
					A *= F_t_u_1[id_no_u.at(i)] + p_u_l[id_no_u.at(i)] - timeProbas_fixed.at(t_fixed)[id_no_u.at(i)]->p(t -l_no_u.at(i));
				}
				p_a_or_b += sign * A;
			} 
			sign *= -1;
		}
		p_a_or_b = p_one - p_a_or_b;


		double p_fixed = p_alpha + p_beta - p_a_or_b;
		if (p_fixed <= ZERO) p_fixed = 0.0;

		p += p_fixed * timeProbas[closest_a->id()]->p(t_fixed);		
		
		// reinitialize
		if (not timeProbas_fixed_already_computed) {
			for (const PSTN_TimeEvent* te: timeEventsToDO) {
				done[te->id()] = false;
			}

			for (const PSTN_TimeEvent* a : timeEventsToDO) {
				if (dep_events.find(a) == dep_events.end()) {
					delete timeProbas_fixed.at(t_fixed)[a->id()];
					timeProbas_fixed.at(t_fixed)[a->id()] = nullptr;
				}
			}
		}
		
	}

	// for (const PSTN_TimeEvent* e_i : vec_dep_events) {
	// // for (const PSTN_TimeEvent* e_i : e) {
	// 	delete timeProbas_fixed[e_i->id()];
	// 	timeProbas_fixed[e_i->id()] = nullptr;
	// }
	// if (timeProbas_fixed[closest_a->id()] != nullptr)
	// 	delete timeProbas_fixed[closest_a->id()];

	return p;
}















double PSTN_Network::P_joint(const PSTN_TimeEvent 														&te_i, 		int x_i, 
							const PSTN_TimeEvent 														&te_h, 		int x_h,
							PSTN_PrDistribution 														**timeProbas,
							map<tuple<const PSTN_TimeEvent*, int, const PSTN_TimeEvent*, int>, double>	&jointProbas) {
	/* If random variables are independent, return simply the product */
	if (not dependent.at(make_pair(&te_i, &te_h))) 
		return timeProbas[te_i.id()]->p(x_i) * timeProbas[te_h.id()]->p(x_h);

	/* Otherwise: check if joint probabilities are already computed */
	if (jointProbas.find(make_tuple(&te_i, x_i, &te_h, x_h)) != jointProbas.end()) {
		// cout << "\t\t\t\t\t --> " << jointProbas.at(make_tuple(&te_i, x_i, &te_h, x_h)) << endl;
		return jointProbas.at(make_tuple(&te_i, x_i, &te_h, x_h));
	}


	
	/* OTHERWISE: make them independent by using total probability theorem on the closest common ancestor */

	
	/* Find the closest (necessarily unpredictable) common ancestor */
	int min_depth = numeric_limits<int>::max();
	const PSTN_TimeEvent* closest_a = nullptr;
	for (auto e: ancestors_with_depth.at(&te_i)) {
		if ((e.first == &te_h or ancestors.at(&te_h).find(e.first) != ancestors.at(&te_h).end()) and e.second < min_depth) {
			min_depth = e.second;
			closest_a = e.first;
		}
	} 	
	ASSERT(closest_a != nullptr, ""); 
	ASSERT(not predictable.at(closest_a), "");


	for (int t_i = 0; t_i <= n_time_units; t_i++) 
		for (int t_h = 0; t_h <= n_time_units; t_h++) {
			jointProbas[make_tuple(&te_i, t_i, &te_h, t_h)] = 0.0;
			jointProbas[make_tuple(&te_h, t_h, &te_i, t_i)] = 0.0;
		}


	// create new time probas, add the closest ancestor, plus all the ancestors that are not shared
	// map<const PSTN_TimeEvent*, PSTN_PrDistribution*> timeProbas_fixed;
	PSTN_PrDistribution* timeProbas_fixed[MAX_TE];
	for (const PSTN_TimeEvent* te: getTimeEvents())
		timeProbas_fixed[te->id()] = nullptr;
	
	timeProbas_fixed[closest_a->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
	timeProbas_fixed[te_h.id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
	timeProbas_fixed[te_i.id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
	for (const PSTN_TimeEvent* a : ancestors.at(&te_i))
		if (ancestors.at(&te_h).find(a) == ancestors.at(&te_h).end())
			timeProbas_fixed[a->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);
	for (const PSTN_TimeEvent* a : ancestors.at(&te_h))
		if (ancestors.at(&te_i).find(a) == ancestors.at(&te_i).end())
			timeProbas_fixed[a->id()] = new PSTN_PrDistribution(n_time_units, CUSTOM_FAST);

	// fill the done indicator map with these time events only
	deque<const PSTN_TimeEvent*> 	timeEventsToDO;
	map<const PSTN_TimeEvent*,bool> done;
	for (const PSTN_TimeEvent* te: getTimeEvents()) {
		if (timeProbas_fixed[te->id()] != nullptr) {
			timeEventsToDO.push_back(te);
			done[te] = false;
		}
	}
		

	// now fill in all the remaining time events, with existing probabilities (existing pointers!)
	for (const PSTN_TimeEvent* te : getTimeEvents()) {
		if (timeProbas_fixed[te->id()] == nullptr) {
			timeProbas_fixed[te->id()] = timeProbas[te->id()];
			if (ancestors.at(&te_i).find(te) != ancestors.at(&te_i).end() or ancestors.at(&te_h).find(te) != ancestors.at(&te_h).end())
				done[te] = true;
		}
	}



	for (int t_fixed = timeProbas[closest_a->id()]->t_min(); t_fixed <= timeProbas[closest_a->id()]->t_max(); t_fixed++) {	
		timeProbas_fixed[closest_a->id()]->p(t_fixed, 1.0);
		done[closest_a] = true;

		bool finished = false;
		while (not finished) {
			const PSTN_TimeEvent &te = *(timeEventsToDO.front()); 
			timeEventsToDO.pop_front(); 
			timeEventsToDO.push_back(&te);

			if (done.at(&te)) continue;

			bool dep_done = true;
			for (const PSTN_TimeEvent* te_prev: prev_time_events.at(&te))
				dep_done = dep_done and done.at(te_prev);

			if (dep_done) {	
				ASSERT(timeProbas_fixed[te.id()]->empty(), te);
				computeProbasTimeEvent(te, timeProbas_fixed);
			
				done[&te] = true;

				finished = true;
				for (const PSTN_TimeEvent *te : timeEventsToDO)
					finished = finished and done.at(te);
			}
		}


		
		for (int t_i = timeProbas_fixed[te_i.id()]->t_min(); t_i <= timeProbas_fixed[te_i.id()]->t_max(); t_i++) {
			for (int t_h = timeProbas_fixed[te_h.id()]->t_min(); t_h <= timeProbas_fixed[te_h.id()]->t_max(); t_h++) {
				jointProbas[make_tuple(&te_i, t_i, &te_h, t_h)] += timeProbas_fixed[te_i.id()]->p(t_i) * timeProbas_fixed[te_h.id()]->p(t_h) * timeProbas[closest_a->id()]->p(t_fixed);
				jointProbas[make_tuple(&te_h, t_h, &te_i, t_i)] += timeProbas_fixed[te_i.id()]->p(t_i) * timeProbas_fixed[te_i.id()]->p(t_h) * timeProbas[closest_a->id()]->p(t_fixed);
			}
		}

		// reinitialize
		for (const PSTN_TimeEvent* te: timeEventsToDO) {
			timeProbas_fixed[te->id()]->clear();
			done[te] = false;
		}
		
	}


	// delete the local objects
	delete timeProbas_fixed[te_h.id()];
	delete timeProbas_fixed[te_i.id()];
	if (closest_a != &te_h and closest_a != &te_i)
		delete timeProbas_fixed[closest_a->id()];
	for (const PSTN_TimeEvent* a : ancestors.at(&te_i))
		if (ancestors.at(&te_h).find(a) == ancestors.at(&te_h).end() and a != &te_h)
			delete timeProbas_fixed[a->id()];
	for (const PSTN_TimeEvent* a : ancestors.at(&te_h))
		if (ancestors.at(&te_i).find(a) == ancestors.at(&te_i).end() and a != &te_i)
			delete timeProbas_fixed[a->id()];

	return jointProbas.at(make_tuple(&te_i, x_i, &te_h, x_h));
}





























Results PSTN_Network::monteCarlo(int n_samples, bool force_cont, int verbose) {
	_ASSERT_(not time_events.empty(), "Empty network! ");

	Results res;
	res.avg_success = 0.0;

	map<const PSTN_TimeEvent*, bool> done;

	for (int i=1; i <= n_samples; i++) {
		if (verbose) cout << endl << "Simulation #" << i << " ------------------------" << endl;
		bool success = true;

		deque<const PSTN_TimeEvent*> timeEventsToDO;
		for (const PSTN_TimeEvent* te: getTimeEvents()) {
			done[te] = false;
			timeEventsToDO.push_back(te);
		}

		map<const PSTN_TimeEvent*, bool> ok;

		int i_cycle = 0;
		const PSTN_TimeEvent* t_leaf = nullptr;
		while (not timeEventsToDO.empty()) {
			_ASSERT_(i_cycle <= (int) timeEventsToDO.size(), "Cycle detected!");

			const PSTN_TimeEvent &te = *(timeEventsToDO.front()); timeEventsToDO.pop_front();
			
			bool dep_done = true;
			for (const PSTN_TemporalConstraint* c : te.getConstraints()) 
				dep_done = dep_done and done.at(c->getTimeEventDependence());

			if (not dep_done) {
				timeEventsToDO.push_back(&te);
				i_cycle++;
			}
			else {	
				i_cycle = 0;
				timeEventValue[&te] = max(0, te.getTW_lb());
				for (const PSTN_TemporalConstraint* c : te.getConstraints()) {
					const PSTN_TimeEvent* with_e = c->getTimeEventDependence();
					
					if (c->getType() == CONTROLLABLE)
						timeEventValue[&te] = max(timeEventValue.at(&te), timeEventValue.at(with_e) + c->getLB());
					else {
						int d = c->sample();
						timeEventValue[&te] = max(timeEventValue.at(&te), timeEventValue.at(with_e) + d);
						if (verbose) cout << "\tSampled duration " << d << " for constraint " << c->getName() << endl;
					}
				}

				bool prev_ok = true;
				for (const PSTN_TemporalConstraint* c : te.getConstraints()) 
					prev_ok = prev_ok and ok.at(c->getTimeEventDependence());

				ok[&te] = prev_ok and te.feasibleTime(timeEventValue.at(&te), timeEventValue);

				success = success and ok.at(&te);

				if (ok.at(&te) or force_cont)
					res.time_probas[&te][timeEventValue.at(&te)] += (double) 1.0 / n_samples;
				// else
				// 	break;

				done[&te] = true;
				if (timeEventsToDO.empty())
					t_leaf = &te;
			}
		}
		res.avg_success += (double) success / n_samples;

	}

	return res;
}































ostream& operator<<(ostream &os, const PSTN_TimeEvent &e) { 
	ostringstream out;

	out << outputMisc::boldExpr() << e.getName() << outputMisc::resetColor() << "(";
	for (const PSTN_TemporalConstraint* c : e.getConstraints())
		out << c->getTimeEventDependence()->getName() << ",";
	out << ")";

	return os << out.str();	
}

ostream& operator<<(ostream &os, const PSTN_TimeEvent *pe) { 
	ostringstream out;
	const PSTN_TimeEvent &e = *pe;

	out << outputMisc::boldExpr() << e.getName() << outputMisc::resetColor() << " \t";
	switch(e.type) {
		case CONTROLLABLE: out 	<< "[Control] "; break; 
		case CONTINGENT: out 	<< "[Contingt]"; break; 
		case MIXED: out 		<< "[Mixed]   "; break; 
		case NOT_SET: _ASSERT_(false, "no type!"); break; 
	}

	if (e.getComment() != "")
		out << " \t" << outputMisc::blueExpr() << e.getComment() << outputMisc::resetColor();
	out << "\t TW: " << e.getTW_lb() << ", " << (e.getTW_ub() < numeric_limits<int>::max() ? to_string(e.getTW_ub()) : "∞");

	for (const PSTN_TemporalConstraint* c : e.getConstraints())
		out << endl << "\u2b06\u2581\u2581\u2581\u2581\u2581  " << outputMisc::boldExpr() << *c << outputMisc::resetColor();

	return os << out.str();	
}



ostream& operator<<(ostream& os, const PSTN_TemporalConstraint& c) {	
	ostringstream out;

	out << c.with_e->getName();

	if (c.type == CONTROLLABLE)
		out << " \t[" << c.lb << ", " << (c.ub < numeric_limits<int>::max() ? to_string(c.ub) : "∞") << "]";
	else
		out << " \t" << outputMisc::cyanExpr() << *c.prob_dist << outputMisc::resetColor() << " \tPDF: \t" << endl << c.prob_dist << endl;

	// if (c.comment != "")
	// 	out << "\t (" << c.comment << ")";

	return os << out.str();
}





ostream& operator<<(ostream& os, const PSTN_Network& e) {
	ostringstream out;

	out << e.getComment() << endl << "#TU: " << e.n_time_units << "    sec/TU: " << e.n_sec_per_TU << endl << "Time events: ";
	for (const PSTN_TimeEvent* te: e.getTimeEvents())
		out << *te << ", ";
	out << endl;

	return os << out.str();
}

ostream& operator<<(ostream& os, const PSTN_Network* pe) {
	ostringstream out;
	const PSTN_Network& e = *pe;

	out << e.getComment() << endl << "# time units: " << e.n_time_units << "    sec/TU: " << e.n_sec_per_TU << endl;
	for (const PSTN_TimeEvent* te: e.getTimeEvents())
		out << te << endl << endl;

	return os << out.str();
}






ostream& operator<<(ostream& os, const Results& stats) {
	ostringstream out;

	for (auto & pair1 : stats.time_probas) {
		double total = 0.0;
		for (auto & pair2 : pair1.second) {
			total += pair2.second * pair1.first->feasibleTW(pair2.first);
			if (pair2.second > 0)
				out << setw(10) << outputMisc::boldExpr() << pair1.first->getName()  << outputMisc::resetColor() << "\t  "
					<< setw(3) << pair2.first << ' ' 
					<< fixed << setprecision(4) << setfill('0') << total << " ~ " << outputMisc::boldExpr(pair1.first->feasibleTW(pair2.first))<< pair2.second  << outputMisc::resetColor() << " " << setfill(' ')
					<< outputMisc::cyanExpr() << outputMisc::redExpr(not pair1.first->feasibleTW(pair2.first)) << string(100 * pair2.second / 1.0, '*') << outputMisc::resetColor() << endl;
		}
		out << endl;
		// out << "\t\t\t\t" << outputMisc::boldExpr() << total << outputMisc::resetColor() << endl;
	}
	if (stats.avg_success >= 0)
		out << "\% success: \t" << 100 * stats.avg_success << endl;
	if (stats.robustness >= 0)
		out << "\% robustness:\t" << 100 * stats.robustness << endl;

	return os << out.str();
}






















void PSTN_Network::load(string filename, string instance_type, int n_decimals, string te_mod_name, double uncertainty) {
	ifstream network_json_file;
	network_json_file.open(filename);
	if (network_json_file.fail()) 
		_ASSERT_(false, "Error: Unable to open file " << filename << endl);
	
	json j_network;
	network_json_file >> j_network;
	network_json_file.close();

	if (instance_type == "Akmal")
		load_Akmal(j_network, filename, n_decimals);
	else if (instance_type == "Mars2020")
		load_Mars2020(j_network, filename, te_mod_name, uncertainty);
	else
		load(j_network);
}

void PSTN_Network::load(json j_network) {
	_ASSERT_(time_events.empty(), "PSTN_Network::load() has already been previously called! " << comment);

	comment = j_network["Comment"];
	n_time_units = j_network["Horizon"]["TimeUnit_n"];
	n_sec_per_TU = j_network["Horizon"]["TimeUnit_nSeconds"];


	for (const auto& j_item : j_network["TimeEvents"].items()) {
		string comment = j_item.value()["Comment"].is_null() ? string() : (const string&) j_item.value()["Comment"];
		PSTN_TimeEvent & e = addTimeEvent(j_item.key(), comment);

		_ASSERT_(not j_item.value()["TimeWindow"][0].is_null() and not j_item.value()["TimeWindow"][1].is_null(), "");
		int ub = j_item.value()["TimeWindow"][1] == "inf" ? numeric_limits<int>::max() : (int) j_item.value()["TimeWindow"][1];
		e.setTimeWindow(j_item.value()["TimeWindow"][0], ub);
	}

	/* controllable edges */
	set<int> controllable_edges_end;
	for (const auto& j_item : j_network["TemporalConstraints"].items()) {
		if (j_item.value()["Type"] != "Controllable") continue;
		
		string comment = j_item.value()["Comment"].is_null() ? string() : (const string&) j_item.value()["Comment"];
		
		PSTN_TimeEvent & from_e = getTimeEvent((const string&) j_item.value()["From"]);
		PSTN_TimeEvent & to_e = getTimeEvent((const string&) j_item.value()["To"]);

		_ASSERT_(not j_item.value()["Type"].is_null(), j_item.key());
		
		// cout << "constraint from " << from_e.getName() << " to " << to_e.getName() << endl;
		
		_ASSERT_(not j_item.value()["Params"]["LB"].is_null() and not j_item.value()["Params"]["UB"].is_null(), j_item.key());
		int ub = j_item.value()["Params"]["UB"] == "inf" ? numeric_limits<int>::max() : (int) j_item.value()["Params"]["UB"];
		addTemporalConstraint(j_item.key(), to_e, from_e, j_item.value()["Params"]["LB"], ub, comment);
		
		controllable_edges_end.insert(to_e.id());
	}

	/* contingent edges */
	for (const auto& j_item : j_network["TemporalConstraints"].items()) {
		if (j_item.value()["Type"] == "Controllable") continue;

		string comment = j_item.value()["Comment"].is_null() ? string() : (const string&) j_item.value()["Comment"];
		
		PSTN_TimeEvent & from_e = getTimeEvent((const string&) j_item.value()["From"]);
		PSTN_TimeEvent & to_e = getTimeEvent((const string&) j_item.value()["To"]);

		_ASSERT_(not j_item.value()["Type"].is_null(), j_item.key());
		
		// cout << "constraint from " << from_e.getName() << " to " << to_e.getName() << endl;
		PSTN_PrDistribution *x;
		if (j_item.value()["Type"] == "Custom") {
			_ASSERT_(not j_item.value()["Distribution"].is_null(), j_item.key());
			_ASSERT_(not j_network["ProbabilityDistributions"][(const string&) j_item.value()["Distribution"]].is_null(), j_item.key());
			
			x = new PSTN_PrDistribution(n_time_units, CUSTOM);

			// map<int, double> dist;
			for (const auto& j_e : j_network["ProbabilityDistributions"][(const string&)j_item.value()["Distribution"]].items())
				x->addCustomProb(stoi(j_e.key()), j_e.value());
				// dist.insert(pair<int,double>(stoi(j_e.key()), j_e.value()));

		}
		else if (j_item.value()["Type"] == "Uniform") {
			_ASSERT_(not j_item.value()["Params"].is_null(), j_item.key());
			_ASSERT_(not j_item.value()["Params"]["Min"].is_null() and not j_item.value()["Params"]["Max"].is_null(), j_item.key());
			
			x = new PSTN_PrDistribution(n_time_units, UNIFORM, (int) j_item.value()["Params"]["Min"], (int) j_item.value()["Params"]["Max"]);
		}
		else if (j_item.value()["Type"] == "PERT") {
			_ASSERT_(not j_item.value()["Params"].is_null(), j_item.key());
			_ASSERT_(not j_item.value()["Params"]["Min"].is_null() and not j_item.value()["Params"]["Max"].is_null() and not j_item.value()["Params"]["Mode"].is_null(), j_item.key());
			
			x = new PSTN_PrDistribution(n_time_units, PERT, (int) j_item.value()["Params"]["Min"], (int) j_item.value()["Params"]["Max"], (int) j_item.value()["Params"]["Mode"]);
		}
		else if (j_item.value()["Type"] == "mod-PERT") {
			_ASSERT_(not j_item.value()["Params"].is_null(), j_item.key());
			_ASSERT_(not j_item.value()["Params"]["Min"].is_null() and not j_item.value()["Params"]["Max"].is_null() and not j_item.value()["Params"]["Mode"].is_null() and not j_item.value()["Params"]["Skew"].is_null(), j_item.key());
			
			x = new PSTN_PrDistribution(n_time_units, MOD_PERT, (int) j_item.value()["Params"]["Min"], (int) j_item.value()["Params"]["Max"], (int) j_item.value()["Params"]["Mode"], (double) j_item.value()["Params"]["Skew"]);
		}
		else if (j_item.value()["Type"] == "Gaussian") {
			_ASSERT_(not j_item.value()["Params"].is_null(), j_item.key());
			_ASSERT_(not j_item.value()["Params"]["Mean"].is_null() and not j_item.value()["Params"]["StdDev"].is_null(), j_item.key());
			
			x = new PSTN_PrDistribution(n_time_units, NORMAL, (int) j_item.value()["Params"]["Mean"], (double) j_item.value()["Params"]["StdDev"]);
		}
		else _ASSERT_(false, "");
		// addTemporalConstraint(j_item.key(), to_e, from_e, *x, comment);
		if (controllable_edges_end.find(to_e.id()) != controllable_edges_end.end()) {
			PSTN_TimeEvent & from_e_bis = addTimeEvent(from_e.getName() + "_bis", "");
			addTemporalConstraint(j_item.key(), from_e_bis, from_e, *x);
			addTemporalConstraint(j_item.key(), to_e, from_e_bis, 0, numeric_limits<int>::max());
		}
		else
			addTemporalConstraint(j_item.key(), to_e, from_e, *x, comment);
		
	}
}






void PSTN_Network::load_Akmal(json j_network, string filename, int n_decimals) {
	_ASSERT_(time_events.empty(), "PSTN_Network::load() has already been previously called! " << comment);

	comment = filename;
	// n_time_units = 1000;
	// n_sec_per_TU = j_network["Horizon"]["TimeUnit_nSeconds"];

	int n_floating_digits = 0, max_n_float = n_decimals;
	double max_time = 0.0;
	for (json j_constraint : j_network["constraints"]) {
		if (j_constraint["type"] == "stc")
			max_time += max ((double) 0.0, (double) j_constraint["min_duration"]);
		else
			max_time += (double) j_constraint["max_duration"];	// TODO: improve: use longuest path

		double x = (double) j_constraint["min_duration"];
		stringstream ss;
		ss << abs(x-(int)x);
		string s;
		ss >> s;
		n_floating_digits = min(max_n_float, max(n_floating_digits, (int) s.length()-2));

		if (j_constraint["max_duration"] != "inf") {
			x = (double) j_constraint["max_duration"];
			stringstream ss_;
			ss_ << abs(x-(int)x);
			s = "";
			ss_ >> s;
			n_floating_digits = min(max_n_float, max(n_floating_digits, (int) s.length()-2));
		}
	}

	if (not j_network["n_decimals"].is_null())
		n_floating_digits = j_network["n_decimals"];
	n_time_units = ceil(max_time * pow(10, n_floating_digits));
	
		
	addTimeEvent("t_0", "");
	for (json j_node : j_network["nodes"]) {
		string name = "t_";
		addTimeEvent(name + to_string((int) j_node["node_id"]), "");
	}

	/* controllable edges */
	bool controllable_edges[MAX_TE];
	for (json j_node : j_network["nodes"])
		controllable_edges[(int) j_node["node_id"]] = false;

	for (json j_constraint : j_network["constraints"]) {
		if (j_constraint["min_duration"] != j_constraint["max_duration"])
			if (j_constraint["type"] == "stcu" or j_constraint["type"] == "stcu_ordinary") continue;

		string name_from = "t_"; name_from += to_string(j_constraint["first_node"]);
		string name_to = "t_"; name_to += to_string(j_constraint["second_node"]);

		PSTN_TimeEvent & from_e = getTimeEvent(name_from);
		PSTN_TimeEvent & to_e = getTimeEvent(name_to);

		double min_duration = (double) j_constraint["min_duration"] * pow(10, n_floating_digits);
		double max_duration = (j_constraint["max_duration"] == "inf" or j_constraint["max_duration"] >= 1000000) ? 
								numeric_limits<int>::max() : (double) j_constraint["max_duration"] * pow(10, n_floating_digits);
		int lb = max(0, (int) ceil(min_duration));
		int ub = floor((double) max_duration);
		_ASSERT_(lb <= ub, lb << "(" << j_constraint["min_duration"] << ")" << " > " << ub << "(" << j_constraint["max_duration"] << ")" << " ! maybe accuracy is not high enough ?");

		string name = "c_"; name += to_string(j_constraint["first_node"]) + "_" + to_string(j_constraint["second_node"]);
		if ((int) j_constraint["first_node"] == 0)
			to_e.setTimeWindow(lb, ub);
		else
			addTemporalConstraint(name, to_e, from_e, lb, ub);
		
		controllable_edges[(int) j_constraint["second_node"]] = true;
	}

	/* contingent edges */
	for (json j_constraint : j_network["constraints"]) {
		if (j_constraint["min_duration"] == j_constraint["max_duration"]) continue;
		if (j_constraint["type"] == "stc") continue;

		string name_from = "t_"; name_from += to_string(j_constraint["first_node"]);
		string name_to = "t_"; name_to += to_string(j_constraint["second_node"]);
		PSTN_TimeEvent & from_e = getTimeEvent(name_from);
		PSTN_TimeEvent & to_e = getTimeEvent(name_to);

		string name = "c_"; name += to_string(j_constraint["first_node"]) + "_" + to_string(j_constraint["second_node"]);

		PSTN_PrDistribution *x;

		if ( j_constraint["type"] == "stcu_ordinary") {
			x = new PSTN_PrDistribution(n_time_units, CUSTOM);
			for (const auto& j_e : j_constraint["ProbabilityDistribution"].items()) {
				// int t = stof(j_e.key()) * pow(10, n_floating_digits);
				int t = stoi(j_e.key());
				// if (name == "c_1_2")
				// 	cout << name << "   " << t << " -> " << j_e.value() << endl;
				x->addCustomProb(t, j_e.value());
			}
		}
		else {
			double min_duration = (double) j_constraint["min_duration"] * pow(10, n_floating_digits);
			double max_duration = (double) j_constraint["max_duration"] * pow(10, n_floating_digits);

			int lb = max(0, (int) ceil(min_duration));
			int ub = ceil((double) max_duration);
			_ASSERT_(lb <= ub, lb << "(" << j_constraint["min_duration"] << ")" << " > " << ub << "(" << j_constraint["max_duration"] << ")" << " ! maybe accuracy is not high enough ?");
			
			x = new PSTN_PrDistribution(n_time_units, UNIFORM, lb, ub);
		}

		if (controllable_edges[(int) j_constraint["second_node"]]) {
			PSTN_TimeEvent & from_e_bis = addTimeEvent(from_e.getName() + "_bis", "");
			addTemporalConstraint(name, from_e_bis, from_e, *x);
			addTemporalConstraint(name, to_e, from_e_bis, 0, numeric_limits<int>::max());
		}
		else
			addTemporalConstraint(name, to_e, from_e, *x);

	
	}
}




void PSTN_Network::load_Mars2020(json j_network, string filename, string te_mod_name, double uncertainty) {
	_ASSERT_(time_events.empty(), "PSTN_Network::load() has already been previously called! " << comment);

	double scale = 10.0;

	comment = filename;
	unsigned long start_time = ceil((double) j_network["planWideData"]["startTime"]);
	unsigned long end_time = floor((double) j_network["planWideData"]["endTime"]);
	n_time_units = end_time - start_time;
	
	n_time_units = ceil(n_time_units / scale);

	map<string, int> real_id_to_id;

	// addTimeEvent("t_0", "");
	int id = 1;
	for (json j_activity : j_network["activities"]) {
		string real_id = to_string(j_activity["activityId"]);
		real_id_to_id[real_id] = id;

		/* activity's start time event */
		PSTN_TimeEvent & te_start = addTimeEvent("t_" + to_string(id), "st. Activ " + real_id);
		/* activity's end time event */
		PSTN_TimeEvent & te_end = addTimeEvent("t_" + to_string(id) + "'", "end Activ " + real_id);
		id++;

		/* set time window */
		if (j_activity["executionTimes"].size()) {
			int lb_start = ceil((double)j_activity["executionTimes"][0]["earliest"] - start_time);	_ASSERT_(lb_start >= 0, j_activity);
			int ub_start = floor((double)j_activity["executionTimes"][0]["latest"] - start_time);	_ASSERT_(ub_start >= 0, j_activity);
			if (lb_start > 0)
				lb_start = ceil(lb_start / scale);
			if (ub_start > 0)
				ub_start = max(lb_start, (int) floor(ub_start / scale));
			_ASSERT_(lb_start <= ub_start, j_activity);
			te_start.setTimeWindow(lb_start, ub_start);
		
			int ub_end = floor((double) j_activity["executionTimes"][0]["cutoff"] - start_time);	_ASSERT_(ub_end >= 0, j_activity);
			ub_end = floor(ub_end / scale);
			_ASSERT_(0 < ub_end, j_activity);
			_ASSERT_(ub_end <= n_time_units, j_activity);
			te_end.setTimeWindow(0, ub_end);
		}

		/* contingent activity's duration */
		json j_activity_dist = j_network["distributions"]["target_activity_duration_distributions"][real_id];
		int mean = j_activity_dist["params"]["mean"];
		double stdev = j_activity_dist["params"]["standard_deviation"];

		mean = ceil(mean / scale);
		stdev /= scale;

		if (uncertainty != 0.0 and te_mod_name == te_end.getName())
			stdev += stdev *(uncertainty / 100.0);


		PSTN_PrDistribution *x = new PSTN_PrDistribution(n_time_units, NORMAL, mean, stdev);
		// PSTN_TemporalConstraint& c = 
		addTemporalConstraint("c_" + to_string(id), te_end, te_start, *x);

		// cout << te_end.getName() << ": " << mean << "   " << stdev << endl;
		// cout << te_end.getName() << ": " << c << endl;
	}

	/* inter-activity time constraints */
	id = 1;
	for (json j_activity : j_network["activities"]) {
		PSTN_TimeEvent & te_start = getTimeEvent("t_" + to_string(id));
		PSTN_TimeEvent & te_end = getTimeEvent("t_" + to_string(id) + "'");
		id++;
		
		for (json j_dependency : j_activity["dependentActivities"]) {
			int code = j_dependency["dependentActivityStatus"];
			
			int with_id = real_id_to_id[to_string(j_dependency["dependentActivityId"])];

			PSTN_TimeEvent & with_te_start = getTimeEvent("t_" + to_string(with_id));
			PSTN_TimeEvent & with_te_end = getTimeEvent("t_" + to_string(with_id) + "'");
			
			string name = "c_" + to_string(id) + "_" + to_string(with_id);
			if (code == 4 or code == 12 or code == 28) {	// the activity's start time must be after with_te_end
				// cout << "add constraint: " << with_te_end << " ≤ " << te_start << endl;
				addTemporalConstraint(name, te_start, with_te_end, 0, numeric_limits<int>::max());
			}
			else if (code == 1) {							// the activity's end time must be before with_te_start
				// cout << "add constraint: " << te_end << " ≤ " << with_te_start << endl;
				addTemporalConstraint(name, with_te_start, te_end, 0, numeric_limits<int>::max());
			}
		}
	}
	
}












