/*
Copyright 2019 Michael Saint-Guillain.
Contact: m.stguillain at gmail dot com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "pstn.h"
#include "prob.h"
#include "tools.h"
#include <cmath>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <getopt.h>
#include <chrono>





void help() {
	cout << "Usage: main [--help] " << endl;
	cout << endl 
		<< "Options:" << endl 
		<< "\t--help (-h) : print this help" << endl
		<< "\t--verbose(+/++) : turn output verbose" << endl
		<< endl
		<< "\t--instance-type (-t) TYPE: type of input JSON file" << endl
		<< "\t--json-input-filename (-I) FILE: the input JSON file describing the network" << endl
		<< "\t--json-ouput-filename (-J) FILE: write results in a JSON file; optional" << endl
		<< endl
		<< "\t--monte-carlo (-M) N: performs MonteCarlo simulation instead, on N samples; optional" << endl
		<< "\t--test (-T) N: compare expectation with Monte Carlo (MC on N samples); optional" << endl
		<< endl
		<< "\t--skip-final: tells the exact computation to not compute overall time probabilities (only interested in time events success probs); optional" << endl
		<< "\t--infos: just print some infos and quit" << endl
		<< endl;
}

int main(int argc, char **argv) {
	if (argc < 2) { help(); return 1; }
	srand(time(NULL));


	// PARAMETERS ------------------------------------------------------------------------------------
	int verbose = 0;
	int montecarlo = 0, test = 0;
	string json_ouput_filename;
	string json_input_filename;
	string instance_type = "";
	int n_decimals = 0;
	int skip_final = 0, infos = 0;

	static struct option long_options[] = {
		{"verbose"				, no_argument		, &verbose 	, 1},
		{"verbose+"				, no_argument		, &verbose 	, 2},
		{"verbose++"			, no_argument		, &verbose 	, 3},
		{"help"					, no_argument		, 0			, 'h'},
		{"json-input-filename"	, required_argument	, 0			, 'I'},
		{"json-ouput-filename"	, required_argument	, 0			, 'J'},
		{"instance-type"		, required_argument	, 0			, 't'},
		{"monte-carlo"			, required_argument	, 0			, 'M'},
		{"test"					, no_argument		, &test		, 100000},
		{"n-decimals"			, required_argument	, 0			, 'D'},
		{"skip-final"			, no_argument		, &skip_final, 1},
		{"infos"				, no_argument		, &infos	, 1},
		{0, 0, 0, 0}
	};
	int c, option_index = 0;
	while ((c = getopt_long (argc, argv, "hI:J:M:t:D:", long_options, &option_index)) != -1 ) {
		vector<string> x;
		switch (c) {
			case 'h': help(); return 1;
			case 'I': json_input_filename = optarg; break;
			case 'J': json_ouput_filename = optarg; break;
			case 't': instance_type = optarg; break;
			case 'M': montecarlo = atoi(optarg); break;
			case 'D': n_decimals = atoi(optarg); break;
			case '?': abort();
		}
	}

	_ASSERT_(json_input_filename != "", "ERROR: required argument --json-input-filename (-I)");
	_ASSERT_(instance_type == "" or instance_type == "Akmal" or instance_type == "Mars2020", "unrecognized instance type: " << instance_type);

	// INITIALIZATION ---------------------------------------------------------------------------------
	PSTN_Network pstn;

	if (verbose) cout << "Loading PSTN input file: " << json_input_filename << " ... " << flush;
	pstn.load(json_input_filename, instance_type, n_decimals);
	pstn.skip_final(skip_final);
	if (verbose) cout << " Done. " << endl << endl;
	cout << endl << "PSTN: " << outputMisc::boldExpr(true) << pstn.getComment() << outputMisc::resetColor() << "\t#TEs: " << pstn.getTimeEvents().size() << "\tTU:" << pstn.getHorizon() << endl;
	if (verbose) cout << pstn << endl;
	if (verbose >= 2) cout << endl << "PSTN:" << endl << &pstn << endl;

	if (infos) {
		for (const PSTN_TimeEvent* te : pstn.getTimeEvents()) 
			cout << *te << " is \t\t" << te->getComment() << endl;
		
		return EXIT_SUCCESS;
	}

	



	Results res_MC;
	int n_samples = (montecarlo?montecarlo:test);
	if (montecarlo or test) {
		cout << "Permoring Monte Carlo simulation (" << n_samples << " samples)..." << endl << flush;
		res_MC = pstn.monteCarlo(n_samples);
		if (test == 0) {
			cout << outputMisc::boldExpr(true) << "\% success: " << res_MC.avg_success * 100.0 << outputMisc::resetColor() << endl;
			if (verbose >= 2) cout << "Statistics:" << endl << res_MC << endl;
		}
	}

	Results res;
	if (montecarlo == 0 or test) {
		cout << "Computing expected robustness..." << (skip_final? " (skip final)":"") << endl << flush;
		res = pstn.DProbustness(verbose);
		if (test == 0) {
			cout << outputMisc::boldExpr(true) << "\% robustness: " << res.robustness * 100.0 << outputMisc::resetColor() << endl;
			// if (verbose >= 2) cout << "Statistics:" << endl << res << endl;
			// for (const PSTN_TimeEvent* te: pstn.getTimeEvents()) {
			// 	double p = 0.0;
			// 	for (int t = 0; t <= pstn.getHorizon(); t++) 
			// 		p += res.time_probas[te][t];
			// 	cout << *te << ": " << p << ", ";
			// }
		}
	}
	if (test and not skip_final) {
		if (res_MC.avg_success == res.robustness or min(res_MC.avg_success, res.robustness) / max(res_MC.avg_success, res.robustness) >= 0.95)
			cout << outputMisc::boolColorExpr(true) << "OK " << outputMisc::resetColor() << " MC: " << res_MC.avg_success << ", E = " << res.robustness;
		else {
			cout << "Wrong! MC: " << res_MC.avg_success << " ≠ E: " << res.robustness;
			cout << "\tTrying MC with " << n_samples * 50 << " (x50) samples..." << flush << endl;
			res_MC = pstn.monteCarlo(n_samples * 50);
			if (res_MC.avg_success == res.robustness or min(res_MC.avg_success, res.robustness) / max(res_MC.avg_success, res.robustness) >= 0.99)
				cout << outputMisc::boolColorExpr(true) << "OK!" << outputMisc::resetColor()  << " MC: " << res_MC.avg_success << ", E = " << res.robustness;
			else
				cout << outputMisc::boolColorExpr(false) << "Wrong!" << " MC: " << res_MC.avg_success << " ≠ E: " << res.robustness << outputMisc::resetColor();
		}
		cout << "\t  relative diff: " << fixed << 1 - min(res_MC.avg_success, res.robustness) / max(res_MC.avg_success, res.robustness) << endl;
	}


	// cout << endl << pstn.getTemporalConstraint("c_1_3").drawSampledPDF(10000000) << endl;
	// cout << pstn.getTemporalConstraint("c_1_2").printCDF() << endl;
	
	// cout << endl << pstn.getTemporalConstraint("c_7_8").drawSampledPDF() << endl;
	// cout << pstn.getTemporalConstraint("c_7_8").printCDF() << endl;

	// cout << endl << pstn.getTemporalConstraint("c_9_10").drawSampledPDF() << endl;
	// cout << pstn.getTemporalConstraint("c_9_10").printCDF() << endl;


	/* Writing solution JSON file */
	if (json_ouput_filename != "") {
		cout << endl << "Writting JSON solution file... : " << json_ouput_filename;
		ofstream json_output_file;
		json j; 
		time_t now = chrono::system_clock::to_time_t(chrono::system_clock::now());
		j["InstanceFile"] = json_input_filename;
		j["N_TE"] = pstn.getTimeEvents().size();
		j["H"] = pstn.getHorizon();
		if (instance_type == "Akmal")
			j["D"] = n_decimals;
		j["CurrentDateTime"] = ctime(& now);

		j["Results_Exact"] = {};
		for (const PSTN_TimeEvent* te : pstn.getTimeEvents()) {
			double p_success = 0.0;
			for (auto e : res.time_probas.at(te))
					p_success += e.second;
			j["Results_Exact"][te->getName()] = round(p_success * 1000000000) / 1000000000;
		}
	
		if (montecarlo) { 
			j["Results_MC"] = {};
			for (const PSTN_TimeEvent* te : pstn.getTimeEvents()) {
				double p_success = 0.0;
				for (auto e : res_MC.time_probas.at(te))
					p_success += e.second;
				j["Results_MC"][te->getName()] = round(p_success * 1000000000) / 1000000000;
			}
		}


		json_output_file.open(json_ouput_filename, ofstream::out | ios::trunc); 
		json_output_file << j.dump(4);
		json_output_file.close();
		cout << "    Done." << endl;
	}
}


