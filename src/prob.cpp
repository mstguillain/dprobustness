/*
Copyright 2019 Michael Saint-Guillain.
Contact: m.stguillain at gmail dot com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/



#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <random>

#include "prob.h"

#include "incbeta.h"
#include "tools.h"
#include "json.hpp"

using json = nlohmann::json;
using namespace std;








PSTN_PrDistribution::~PSTN_PrDistribution() {
	if (generator != nullptr)
		delete generator;

	if (normal_dist != nullptr)
		delete normal_dist;
	if (uniform_dist != nullptr)
		delete uniform_dist;

	if (pdf != nullptr)
		delete pdf;
	if (cdf != nullptr)
		delete cdf;
	
}


/* UNIFORM */
PSTN_PrDistribution::PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int x_min, int x_max) {
	_ASSERT_(type == UNIFORM, "");
	_ASSERT_(0 <= x_min and x_min <= x_max and x_max <= n_TU_horizon, 
		"x_min: " << x_min << ", x_max: " << x_max << ", H: " << n_TU_horizon);
	
	this->n_TU_horizon 	= n_TU_horizon;
	this->type 			= UNIFORM;
	this->x_min			= x_min;
	this->x_max			= x_max;

	uniform_dist = new uniform_int_distribution<>(x_min, x_max);

	init();
}

/* PERT */
PSTN_PrDistribution::PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int x_min, int x_max, int mode) {
	_ASSERT_(type == PERT, "");
	_ASSERT_(0 <= x_min and x_min <= mode and mode <= x_max and x_max <= n_TU_horizon, 
		"x_min: " << x_min << ", mode: " << mode << ", x_max: " << x_max << ", H: " << n_TU_horizon);
	
	this->n_TU_horizon 	= n_TU_horizon;
	this->type 			= PERT;
	this->x_min			= x_min;
	this->x_max			= x_max;
	this->mode			= mode;
	this->skew			= 4;

	// cout << endl << "x_min: " << x_min << ", mode: " << mode << ", x_max: " << x_max << ", H: " << n_TU_horizon << endl << endl;

	init();
}

/* MOD_PERT */ 	
PSTN_PrDistribution::PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int x_min, int x_max, int mode, double skew) {
	_ASSERT_(type == MOD_PERT, "");
	_ASSERT_(0 <= x_min and x_min <= mode and mode <= x_max and x_max <= n_TU_horizon, 
		"x_min: " << x_min << ", mode: " << mode << ", x_max: " << x_max << ", H: " << n_TU_horizon);
	_ASSERT_(0 < skew and skew <= 50, skew);
	
	this->n_TU_horizon 	= n_TU_horizon;
	this->type 			= MOD_PERT;
	this->x_min			= x_min;
	this->x_max			= x_max;
	this->mode			= mode;
	this->skew			= skew;

	init();
}

/* NORMAL */
PSTN_PrDistribution::PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int mean, double stdev) {
	_ASSERT_(type == NORMAL, "");
	_ASSERT_(n_TU_horizon > 0, n_TU_horizon);
	_ASSERT_(0 <= mean and mean <= n_TU_horizon, "mean: "<< mean << ", H: " << n_TU_horizon);
	_ASSERT_(0 <= stdev and stdev <= n_TU_horizon, "stdev: "<< stdev << ", H: " << n_TU_horizon);
	
	this->n_TU_horizon 	= n_TU_horizon;
	this->type 			= NORMAL;
	this->mean			= mean;
	this->stdev			= stdev;

	x_min 				= 0;
	x_max 				= n_TU_horizon;
	
	init();
}


/* CUSTOM and CUSTOM_FAST */
PSTN_PrDistribution::PSTN_PrDistribution(int n_TU_horizon, DistributionType type) {
	_ASSERT_(type == CUSTOM or type == CUSTOM_FAST, "");
	_ASSERT_(n_TU_horizon > 0, n_TU_horizon);
	// _ASSERT_(dist.size() >= 1, "");

	this->n_TU_horizon 	= n_TU_horizon;
	this->type 			= type;

	x_min 				= n_TU_horizon+1;
	x_max 				= -1;

	init();

}

void PSTN_PrDistribution::addCustomProb(int t, double pr) {
	_ASSERT_(type == CUSTOM, "");

	p(t, pr);
	preComputeCumulative();
}

void PSTN_PrDistribution::p_add(int t, double pr) {
	ASSERT(0 <= t and t <= n_TU_horizon, t << " " << n_TU_horizon);
	ASSERT(pr >= 0 and pr <= 1.0, pr);

	p(t, pdf[t] + pr);
}
void PSTN_PrDistribution::p(int t, double pr) {
	ASSERT(type == CUSTOM or type == CUSTOM_FAST, "");

	ASSERT(0 <= t and t <= n_TU_horizon, t << " " << n_TU_horizon);
	ASSERT(pr >= 0 and pr <= 1.0, pr);

	// double diff = pr - pdf[t];
	pdf[t] = pr;

	if (t < x_min and pr > ZERO) 			// Update x_min
		x_min = t;
	else if (t == x_min and pr <= ZERO) 
		updateMinBound(t+1);

	if (t > x_max and pr > ZERO) 			// Update x_max
		x_max = t;
	else if (t == x_max and pr <= ZERO) 
		updateMaxBound(t-1);

	// for (int x = t; x <= n_TU_horizon; x++)
	// 	cdf[x] += diff;
	// if (x_max >=0)
	// 	ASSERT(cdf[x_max] <= 1.0, cdf[x_max]);

}
double PSTN_PrDistribution::p(int t) const {
	ASSERT(0 <= t and t <= n_TU_horizon, t << " " << n_TU_horizon);
	ASSERT(pdf[t] >= 0 and pdf[t] <= 1.0, pdf[t]);

	return pdf[t];
}


void PSTN_PrDistribution::init() {
	if (type != CUSTOM_FAST) {
		if (type != UNIFORM)
			uniform_dist = new uniform_int_distribution<>(1, 10000000);	// for sampling
		if (generator == nullptr) {
			unsigned seed = chrono::system_clock::now().time_since_epoch().count();
			generator = new default_random_engine(seed);
		}
	}

	pdf = new double[n_TU_horizon+1];
	fill_n(pdf, n_TU_horizon + 1, 0.0);
	cdf = new double[n_TU_horizon+1];
	fill_n(cdf, n_TU_horizon + 1, 0.0);

	if (type != CUSTOM_FAST)
		preComputeCumulative();
	if (type != CUSTOM and type != CUSTOM_FAST)
		preComputeProbs();

	updateMinBound(0);
	updateMaxBound(n_TU_horizon);
}
void PSTN_PrDistribution::updateMinBound(int start) {
	int x;
	for (x = start; x <= n_TU_horizon; x++)
		if (pdf[x] > ZERO) break;
	x_min = x;
}
void PSTN_PrDistribution::updateMaxBound(int start) {
	int x;
	for (x = start; x >= 0; x--)
		if (pdf[x] > ZERO) break;
	x_max = x;
}

void PSTN_PrDistribution::clear() {
	ASSERT(type == CUSTOM_FAST, "");

	for (int x = x_min; x <= x_max; x++) {
		pdf[x] = 0.0;
		cdf[x] = 0.0;
	}
	x_min = n_TU_horizon+1;
	x_max = -1;
}


void PSTN_PrDistribution::preComputeProbs() {
	double last_F = 0;
	for (int i = 0; i <= n_TU_horizon; i++) {
		double p = F(i) - last_F;
		last_F = F(i);
		if (p > 0)
			pdf[i] = p;
	}
}

void PSTN_PrDistribution::preComputeCumulative() {
	

	if (type == CUSTOM or type == CUSTOM_FAST) {
		double p = 0;
		for (int i=0; i <= n_TU_horizon; i++) {
			p += pdf[i];
			if (pdf[i] > 0)
				cdf[i] = p;
		}
	}
	else if (type == NORMAL) {
		for (int x = 0; x <= n_TU_horizon; x++) {
			double p = 0.5 * erfc(- (x-mean) / (stdev * sqrt(2)));
			cdf[x] = p;
		}
	}
	else if (type == UNIFORM) {
		double p = 0;
		for (int x = x_min; x <= x_max; x++) {
			p += (double) 1 / ((x_max - x_min + 1));
			cdf[x] = p;
		}
	}

	else if (type == MOD_PERT or type == PERT) {
		x_min = max(x_min-1, 0);

		double mu = (double) (x_min + x_max + skew * mode) / (skew + 2);
		double v = ((mu - x_min) * (2*mode - x_min - x_max)) / ( (mode - mu) * (x_max - x_min) );
		double w = (v * (x_max - mu)) / (mu - x_min);

		for (int x = x_min; x <= x_max; x++) {
			double z = (double) (x - x_min) / (x_max - x_min);
			cdf[x] = incbeta(v, w, z);
		}
	}
	else _ASSERT_(false, "");
}

double PSTN_PrDistribution::F(int x) const {
	if (x < 0 or x_max < 0) return 0.0;
	// ASSERT(x <= n_TU_horizon, *this << "x:" << x << "  H:" << n_TU_horizon);

	if (type == CUSTOM_FAST) {
		double total = 0.0;
		for (int x_ = x_min; x_ <= min(n_TU_horizon, min(x, x_max)); x_++)
			total += pdf[x_];
		return total;
	}

	return cdf[min(x, n_TU_horizon)];
}




int PSTN_PrDistribution::sample() const {
	ASSERT(type != CUSTOM_FAST, "");

	double s = -1;
	switch(type) {	  
		case UNIFORM: 	 
			s = (*uniform_dist)(*generator); 
			break;
		case PERT: 
		case MOD_PERT: 	
		case CUSTOM:  
		case CUSTOM_FAST:
		case NORMAL: {
			double F_last = 0.0; 		
			s = (*uniform_dist)(*generator) / (double) 10000000; 
			// for (pair<int, double> e : cdf) {
			for (int i = 0; i <= n_TU_horizon; i++) {
				if (F_last < s and s <= cdf[i]) {
					s = i;
					break;
				}
				F_last = cdf[i];
			}
			break; 
		}
		// case UNIFORM: 		s = (*uniform_dist)(*generator); break;
		// case NORMAL: 		s = (*normal_dist)(*generator); break;	
		case NO_TYPE: _ASSERT_(false, "no type!"); break; 
	}
	return s;
}











void PSTN_PrDistribution::printSamples(int n) const {
	for (int i =0; i < n; i++)
		cout << sample() << " ";
}

const string& PSTN_PrDistribution::drawSampledPDF(int n) const {
	ostringstream out;
	
	map<int, int> hist;
	for (int i=0; i<n; ++i) 
		++hist[round(sample())];
	
	for (auto p : hist) {
		out	<< " \t" << std::setw(3) << p.first << ' ' 
				<< fixed << setprecision(4) << setfill('0') << outputMisc::boldExpr(p.second > 0.0)<< (float) p.second / n << outputMisc::resetColor() << " " << setfill(' ')
				<< outputMisc::cyanExpr() << std::string(p.second / std::max(1, (int) n / 300), '*') << outputMisc::resetColor() << endl;
	}

	static string str = ""; str = out.str();
	return (str);
}

const string& PSTN_PrDistribution::printPDF() const {
	ostringstream out;
	
	for (int i=0; i <= n_TU_horizon; i++) 
		if (pdf[i] > 0) out << outputMisc::resetColor() << i << ":" << outputMisc::cyanExpr() << pdf[i] << outputMisc::resetColor() << "  ";

	static string str = ""; str = out.str();
	return (str);
}

const string& PSTN_PrDistribution::printCDF() const {
	_ASSERT_(type != CUSTOM_FAST, "");
	ostringstream out;
	
	out << "CDF: ";
	for (int i = 0; i <= n_TU_horizon; i++)
		if (cdf[i] > ZERO)
		out << outputMisc::resetColor() << i << ":" << outputMisc::cyanExpr() << cdf[i] << outputMisc::resetColor() << "  ";

	static string str = ""; str = out.str();
	return (str);
}



ostream& operator<<(ostream& os, const PSTN_PrDistribution& x) {
	ostringstream out;
	switch(x.type) {
		case CUSTOM: 	out << "Custom."; break;
		case CUSTOM_FAST: out << "Custom+."; break;
		case UNIFORM: 	out << "U(" << x.x_min << ", " << x.x_max << ")"; break;
		case NORMAL: 	out << "N(" << x.mean << ", " << x.stdev << ")"; break;
		case PERT: 		out << "P(" << x.x_min << ", " << x.mode << ", " << x.x_max << ")"; break;
		case MOD_PERT: 	out << "mP(" << x.x_min << ", " << x.mode << ", " << x.x_max << ", " << x.skew << ")"; break;
		case NO_TYPE: _ASSERT_(false, "no type!"); break; 
	}
	return os << out.str();
}

ostream& operator<<(ostream& os, const PSTN_PrDistribution* px) {
	ostringstream out;
	double total = 0.0;
	_ASSERT_(px->x_min == 0 or px->pdf[px->x_min-1] <= ZERO, px->x_min << ": " << px->pdf[px->x_min-1]);
	_ASSERT_(px->x_max == px->n_TU_horizon or px->pdf[px->x_max+1] <= ZERO, px->x_max << ": " << px->pdf[px->x_max-1]);
	for (int i = px->x_min; i <= px->x_max; i++) {
		if (px->pdf[i] > 0)
			out << " \t" << outputMisc::cyanExpr() << std::setw(3) << i << outputMisc::resetColor() << ' ' 
				<< fixed << setprecision(4) << setfill('0') << px->F(i) << " ~ " << outputMisc::boldExpr(px->pdf[i] > 0.0) << px->pdf[i] << outputMisc::resetColor() << " " << setfill(' ')
				<< outputMisc::cyanExpr() << std::string(100 * px->pdf[i] / 1.0, '*') << outputMisc::resetColor() << endl;
		total += px->pdf[i];
	}
	if (total < 0.9999999)
		out << "\t\t\t" << setprecision(10) << total << endl;
	return os << out.str();
}










