/*
Copyright 2019 Michael Saint-Guillain.
Contact: m.stguillain at gmail dot com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/



#ifndef PROB_H
#define	PROB_H


#include <iostream>
#include <vector>
#include <limits>
#include <set>
#include <map>
#include <cmath>
#include <random>
#include "tools.h"
#include "json.hpp"

using json = nlohmann::json;
using namespace std;

class PSTN_PrDistribution;




enum DistributionType {	
	UNIFORM			,
	NORMAL			,
	PERT			,
	MOD_PERT		,
	CUSTOM			,
	CUSTOM_FAST		,
	NO_TYPE		
};
static double ZERO = 0.0;				// probability threshold; below, its improbable (=0.0). 
// static double ZERO = 0.00000000000001;	// probability threshold; below, its improbable (=0.0). 
// static double ZERO = 0.000000001;	// probability threshold; below, its improbable (=0.0). 


class PSTN_PrDistribution {
protected:
	DistributionType type = NO_TYPE;

	int 	n_TU_horizon;
	// int 	nb_TU_prob = 1;

	int 	x_min;			// UNIFORM, PERT, MOD_PERT		but also CUSTOM_FAST (dynamically maintained)
	int 	x_max;			// UNIFORM, PERT, MOD_PERT		but also CUSTOM_FAST (dynamically maintained)
	int 	mean;			// NORMAL
	int 	mode;			// PERT, MOD_PERT
	double 	stdev;			// NORMAL
	double 	skew;			// MOD_PERT

	default_random_engine 		*generator 		= nullptr;
	normal_distribution<>		*normal_dist 	= nullptr;
	uniform_int_distribution<>	*uniform_dist 	= nullptr;

	double 						*pdf = nullptr;	
	double 						*cdf = nullptr;	

public:
	PSTN_PrDistribution(int n_TU_horizon, DistributionType type);												// CUSTOM, CUSTOM_FAST
	PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int x_min, int x_max);							// UNIFORM
	PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int x_min, int x_max, int mode);				// PERT
	PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int x_min, int x_max, int mode, double skew);	// MOD_PERT
	PSTN_PrDistribution(int n_TU_horizon, DistributionType type, int mean, double stdev);						// NORMAL
	~PSTN_PrDistribution();

	void 	clear();							// CUSTOM_FAST
	bool 	empty() const { return x_min > x_max; }

	void 	addCustomProb(int t, double p);		// CUSTOM
	double 	p(int t) const;	
	void 	p(int t, double pr);				// CUSTOM_FAST
	void 	p_add(int t, double pr);			// CUSTOM_FAST
	double 	F(int x) const;

	int 	t_min() const { return x_min; }
	int 	t_max() const { return x_max; }

	int 	sample() const;

	const double* 			getPDF() const { return pdf; }
	friend ostream& operator<<(ostream& os, const PSTN_PrDistribution& x); 		// prints the dist. type & parameters
	friend ostream& operator<<(ostream& os, const PSTN_PrDistribution* px);  	// draws (ACSII art) the PDF
	const string& drawSampledPDF(int n = 10000000) const;
	// const string& drawPDF() const;
	const string& printPDF() const;
	const string& printCDF() const;

protected:
	void init();
	void preComputeProbs();
	void preComputeCumulative();
	void updateMinBound(int start);
	void updateMaxBound(int start);
	void printSamples(int n) const;
};













#endif