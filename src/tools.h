/*
Copyright 2019 Michael Saint-Guillain.
Contact: m.stguillain at gmail dot com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <iostream>
#include <fstream>	// file I/O
#include <sstream>	// ostringstream
#include <cstdlib>	// rand
#include <ctime>	// time
#include <cmath>	// sqrt
#include <cstring>	// memcpy
#include <limits>   // numeric_limits
#include <iomanip>  // std::setw
#include <set>		// set<int>
#include <algorithm>// std::swap, std::max
#include <vector>	// std::vector

using namespace std;

#ifndef TOOLS_H
#define	TOOLS_H

/* an assert statement, but with message */
#ifdef ASSERTS
#define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
    } while (false)
#else
#define ASSERT(condition, message) do { } while (false)
#endif

#define _ASSERT_(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
    } while (false)


#define LINE "------------------------------------------------------------------"


/* Useful quiet the "Unused parater" message at compilation */
#define UNUSED(expr) (void)(expr);


template <typename T>
inline void shuffleArray(T* array, int size, int iter) {
	T tmp;
	int x;
	for(int j=0; j<iter; j++)
		for(int i=0; i<size; i++) {
			x = rand() % size;
			tmp = array[i];
			array[i] = array[x];
			array[x] = tmp;
		}
}


template <typename T>
inline int argmin(const T* array, int from, int to, vector<int>* ties = nullptr) {
	T min = numeric_limits<T>::max();
	int min_idx = -1;

	for (int i=from; i <= to; i++) {
		// cout << array[i] << " - ";
		if (array[i] == min and ties)
			ties->push_back(i);

		if (array[i] < min) {
			min = array[i];
			min_idx = i;
			if (ties) {
				ties->clear();
				ties->push_back(i);
			}
		}
	}
	// cout << " MIN= " << array[min_idx] << " (" << min_idx << ")" << endl;
	return min_idx;
}


/* miscellaous output functions */
struct outputMisc {
	inline static const string boolColorExpr(bool expr) {	// set color to green if expr==true, red otherwise
		if (expr) return "\033[0;32m";
		else return "\033[0;31m"; 
	}
	inline static const string redExpr(bool expr = true) {
		if (expr) return "\033[0;31m";
		else return ""; 
	}
	inline static const string magentaExpr(bool expr = true) {
		if (expr) return "\033[0;35m";
		else return ""; 
	}
	inline static const string cyanExpr(bool expr = true) {
		if (expr) return "\033[0;36m";
		else return ""; 
	}
	inline static const string greenExpr(bool expr = true) {
		if (expr) return "\033[0;32m";
		else return ""; 
	}
	inline static const string blueExpr(bool expr = true) {
		if (expr) return "\033[0;34m";
		else return ""; 
	}
	inline static const string whiteExpr(bool expr = true) {
		if (expr) return "\033[1;37m";
		else return ""; 
	}
	inline static const string redBackExpr(bool expr = true) {
		if (expr) return "\033[0;41m";
		else return ""; 
	}
	inline static const string blueBackExpr(bool expr = true) {
		if (expr) return "\033[0;44m";
		else return ""; 
	}
	inline static const string boldExpr(bool expr = true) {
		if (expr) return "\e[1m";
		else return ""; 
	}
	inline static const string resetColor() {
		return "\033[0;0m";
	}
};



// std::ostream& bold_on(std::ostream& os) { return os << "\e[1m"; }
// std::ostream& bold_off(std::ostream& os) { return os << "\e[0m"; }
// std::ostream& reset_color(std::ostream& os) { return os << "\033[0;0m"; }





template <typename T>
vector<string> split(string s_, char del) {
	vector<string> strings;
	istringstream f(s_);
	string s;    
	while (getline(f, s, del)) strings.push_back(s);
	return strings;
}



/*
to convert any type into string; eg:
string s = "The meaning is " + Str( 42 );
*/
template <typename T>
string Str( const T & t ) {
   ostringstream os;
   os << t;
   return os.str();
}








/* generate all the subsets of fixed size subset_size from the set {1,...,n} to the vector subsets 
	using efficient method described here: http://graphics.stanford.edu/~seander/bithacks.html#NextBitPermutation
	encoding: bitset 	*/
inline vector<unsigned int> generate_subsets(int n, int subset_size) {
	const int max_bits = sizeof(unsigned int) * 8;
	_ASSERT_(n <= max_bits, "");

	vector<unsigned int> subsets;

	unsigned int v = (1 << subset_size) - 1; // current permutation of bits 
	unsigned int w = 0; // next permutation of bits
	unsigned int max = (1 << n) - (1 << (n-subset_size));

	while (v != max) {
		subsets.push_back(v);
		
		unsigned int t = v | (v - 1); // t gets v's least significant 0 bits set to 1
		// Next set to 1 the most significant bit to change, 
		// set to 0 the least significant ones, and add the necessary 1 bits.
		w = (t + 1) | (((~t & -~t) - 1) >> (__builtin_ctz(v) + 1));  	
		
		v = w;
	}
	subsets.push_back(v);
	return subsets;
}


// string& bitset32_toString(unsigned int s) {
// 	ostringstream out; 
// 	// out << bitset<32>(s) << ":  ";
// 	for (int i = 0; i < (int) sizeof(unsigned int) * 8; i++)
// 		if ((1 << i) & s) 
// 			out << i+1 << "  ";
// 	static string str = ""; str = out.str();
// 	return (str);
// }












#endif