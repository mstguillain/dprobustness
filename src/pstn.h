/*
Copyright 2019 Michael Saint-Guillain.
Contact: m.stguillain at gmail dot com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/



#ifndef PSTN_H
#define	PSTN_H


#include <iostream>
#include <vector>
#include <limits>
#include <deque>
#include <map>
#include <cmath>
#include "tools.h"
#include "json.hpp"


#include "prob.h"

using json = nlohmann::json;
using namespace std;

class PSTN_TimeEvent;
class PSTN_TemporalConstraint;
class PSTN_Network;





enum Type {	
	CONTROLLABLE	,
	CONTINGENT		,
	MIXED			,
	NOT_SET		
};






class PSTN_TimeEvent {
protected:
	string 		name;
	int 		id_ 	= -42;
	Type 		type 	= Type::CONTROLLABLE;		// By default; updated automatically upon calls of addConstraint()
	int 		lb 		= 0;
	int 		ub 		= numeric_limits<int>::max();		
	
	vector<const PSTN_TemporalConstraint*> constraints;					// Incoming edges
	
	string comment;

public:
	PSTN_TimeEvent(string name, int id_, string comment = string());
	~PSTN_TimeEvent();
	void 	setComment(string c);
	void 	setTimeWindow(int lb, int ub);
	
	inline int id() const { return id_; };

	string 	getName() const { return name; }
	string 	getComment() const { return comment; }
	int 	getType() const { return type; }
	int 	getTW_lb() const { return lb; }
	int 	getTW_ub() const { return ub; }
	bool 	feasibleTime(int t, const map<const PSTN_TimeEvent*, int> & timeEventValue) const;
	bool 	feasibleTW(int t) const { return lb <= t and t <= ub; }
	
	const vector<const PSTN_TemporalConstraint*> & getConstraints() const { return constraints; }

	friend ostream& operator<<(ostream& os, const PSTN_TimeEvent& e);  
	friend ostream& operator<<(ostream& os, const PSTN_TimeEvent* pe);  

protected:
	PSTN_TemporalConstraint& addConstraint(string name, const PSTN_TimeEvent& with_e, int lb, int ub, string comment = string());
	PSTN_TemporalConstraint& addConstraint(string name, const PSTN_TimeEvent& with_e, const PSTN_PrDistribution& x, string comment = string());
	void 					removeConstraint(PSTN_TemporalConstraint& c) ;
	friend class PSTN_Network;
};



class PSTN_TemporalConstraint {
protected:
	string 						name;
	Type 						type 		= NOT_SET;
	const PSTN_TimeEvent* 		with_e 		= nullptr;

	int 						lb = -1; 
	int 						ub = -1;
	const PSTN_PrDistribution* 	prob_dist 	= nullptr;

	string 						comment;

public:
	PSTN_TemporalConstraint(string name, const PSTN_TimeEvent& with_e, int lb, int ub, string comment = string());
	PSTN_TemporalConstraint(string name, const PSTN_TimeEvent& with_e, const PSTN_PrDistribution& x, string comment = string());
	~PSTN_TemporalConstraint();

	string 	getName() const { return name; }
	string 	getComment() const { return comment; }
	int 	getType() const { return type; }
	int 	getLB() const;
	int 	getUB() const;

	double 	getProbDuration(int t) const;
	
	const PSTN_TimeEvent* getTimeEventDependence() const;	// returns with_e

	int 	sample() const;

	const double* 				getPDF() const;
	const string& 				printPDF() const;
	const string& 				printCDF() const;
	const string& 				drawSampledPDF(int n = 10000000) const;
	friend ostream& operator<<(ostream& os, const PSTN_TemporalConstraint& c);  
};



struct Results {	
	double avg_success = -1;
	double robustness = -1;
	map<const PSTN_TimeEvent*, map<int, double>> time_probas;
	friend ostream& operator<<(ostream& os, const Results& stats);  
};

class PSTN_Network {
protected:
	string 	comment = string();
	int 	n_time_units = -1;
	int 	n_sec_per_TU = -1;
	bool 	skip_t_F = false;

	vector<PSTN_TimeEvent *> 			time_events;
	vector<PSTN_TemporalConstraint *> 	time_constraints;

public:
	PSTN_Network();
	~PSTN_Network();

	static const int MAX_TE = 1000;

	string 	getComment() const { return comment; }
	int 	getHorizon() const { return n_time_units; }

	void load(string filename, string instance_type = "", int n_decimals = 0, string te_mod_name = "", double uncertainty = 0.0);
	void skip_final(bool skip = true) { skip_t_F = skip; }
	
	PSTN_TimeEvent& 				addTimeEvent(string name, string comment = string());
	PSTN_TemporalConstraint& 		addTemporalConstraint(string name, PSTN_TimeEvent& to_te, const PSTN_TimeEvent& with_te, int lb, int ub, string comment = string());
	PSTN_TemporalConstraint& 		addTemporalConstraint(string name, PSTN_TimeEvent& to_te, const PSTN_TimeEvent& with_te, const PSTN_PrDistribution& x, string comment = string());

	PSTN_TimeEvent& 				getTimeEvent(string name);
	const vector<PSTN_TimeEvent*> & getTimeEvents() const { return time_events; }

	PSTN_TemporalConstraint& 					getTemporalConstraint(string name);
	const vector<PSTN_TemporalConstraint*> & 	getTemporalConstraints() const { return time_constraints; }

	Results monteCarlo(int n_samples = 100000, bool force_cont = false, int verbose = 0);
	Results DProbustness(int verbose = 0);

	friend ostream& operator<<(ostream& os, const PSTN_Network& e);  
	friend ostream& operator<<(ostream& os, const PSTN_Network* pe);  

protected:
	void findDependencies(int verbose = 0);

	double f(const PSTN_TimeEvent &te, int t, PSTN_PrDistribution** timeProbas);
	double g(const PSTN_TimeEvent &te, int t, PSTN_PrDistribution** timeProbas, map<int, PSTN_PrDistribution**> &timeProbas_fixed);
	double g_F(const PSTN_TimeEvent &te, PSTN_PrDistribution** timeProbas, map<int, PSTN_PrDistribution**> &timeProbas_fixed);
	double P_joint(const PSTN_TimeEvent &te_i, int x_i, const PSTN_TimeEvent &te_h, int x_h, PSTN_PrDistribution** timeProbas, map<tuple<const PSTN_TimeEvent*, int, const PSTN_TimeEvent*, int>, double> &jointProbas);
	
	vector<const PSTN_TimeEvent*> 								leaf_time_events;
	vector<const PSTN_TimeEvent*> 								root_time_events;
	map<const PSTN_TimeEvent*, vector<const PSTN_TimeEvent*>> 	next_time_events;
	map<const PSTN_TimeEvent*, vector<const PSTN_TimeEvent*>> 	prev_time_events;
	map<pair<const PSTN_TimeEvent*, const PSTN_TimeEvent*>, const PSTN_TemporalConstraint*> 	constraint;
	set<pair<const PSTN_TimeEvent*, const PSTN_TimeEvent*>> 									skip_constraint;
	
	map<const PSTN_TimeEvent*, int> 						timeEventValue; 		// Monte Carlo
	PSTN_PrDistribution*									timeEventProbas[MAX_TE];		// array
					
	map<pair<const PSTN_TimeEvent*, const PSTN_TimeEvent*>, bool> 		dependent;
	map<const PSTN_TimeEvent*, set<const PSTN_TimeEvent*>> 				ancestors;
	map<const PSTN_TimeEvent*, set<pair<const PSTN_TimeEvent*, int>>> 	ancestors_with_depth;
	map<pair<const PSTN_TimeEvent*, const PSTN_TimeEvent*>, int>		distance;
	map<const PSTN_TimeEvent*, bool> 									predictable;


	void load(json j_network);
	void load_Akmal(json j_network, string filename, int n_decimals);
	void load_Mars2020(json j_network, string filename, string te_mod_name = "", double uncertainty = 0.0);

private:
	bool rec_predict(const PSTN_TimeEvent* curr_te);
	void rec_ancestors(const PSTN_TimeEvent* curr_te, const PSTN_TimeEvent* start_te, int depth=0);


	void computeProbasTimeEvent(const PSTN_TimeEvent& te, PSTN_PrDistribution** timeEventProbas);

	double max_timeEvents(vector<const PSTN_TimeEvent*> &e, vector<int> &l, vector<int> &u, int t, PSTN_PrDistribution** timeProbas, map<int, PSTN_PrDistribution**> &timeProbas_fixed);

	void spotExtraConstraints(bool verbose = false);
};












#endif

